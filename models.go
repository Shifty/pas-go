// Legacy and slash command models(slash)/maps(legacy)
package main

import (
	"main/cmd"
	"main/core"

	"github.com/bwmarrin/discordgo"
)

// I wanted to put this file in literally any of the directories
// but couldn't due to import cycling... so here it is

// Slash command desciptions are simplified to fit the 110 character limit.

var (
	dmPermission = false
	SlashModels  = []*discordgo.ApplicationCommand{
		{
			Name:         "about",
			Description:  "Displays information about the bot.",
			DMPermission: &dmPermission,
		},
		{
			Name:         "clearqueue",
			Description:  "Clears the entire queue. This does not stop the current song.",
			DMPermission: &dmPermission,
		},
		{
			Name:         "connect",
			Description:  "Connects the bot to a channel.",
			DMPermission: &dmPermission,
		},
		{
			Name:         "disconnect",
			Description:  "Disconnects the bot from a channel and clears the queue.",
			DMPermission: &dmPermission,
		},
		{
			Name:         "donate",
			Description:  "Shows donation information for Pas.",
			DMPermission: &dmPermission,
		},
		{
			Name:        "evaluate",
			Description: "Executes raw JavaScript code. Use with caution.",
			Options: []*discordgo.ApplicationCommandOption{
				{
					Type:        discordgo.ApplicationCommandOptionString,
					Name:        "code",
					Description: "Code to execute",
					Required:    true,
				},
			},
			DMPermission: &dmPermission,
		},
		{
			Name:        "help",
			Description: "Shows information on the specified command. Omit arguments to show the list command list.",
			Options: []*discordgo.ApplicationCommandOption{
				{
					Type:        discordgo.ApplicationCommandOptionString,
					Name:        "query",
					Description: "Command query",
					Required:    false,
				},
			},
			DMPermission: &dmPermission,
		},
		{
			Name:         "history",
			Description:  "Shows the last 10 songs played.",
			DMPermission: &dmPermission,
		},
		{
			Name:        "move",
			Description: "Moves the target song to the target position in the queue. Specify both by index.",
			Options: []*discordgo.ApplicationCommandOption{
				{
					Type:        discordgo.ApplicationCommandOptionInteger,
					Name:        "current-position",
					Description: "Current position of the song in the queue",
					Required:    true,
				},
				{
					Type:        discordgo.ApplicationCommandOptionInteger,
					Name:        "target-position",
					Description: "Target position in the queue",
					Required:    true,
				},
			},
			DMPermission: &dmPermission,
		},
		{
			Name:         "nowplaying",
			Description:  "Shows the currently playing song.",
			DMPermission: &dmPermission,
		},
		{
			Name:         "pause",
			Description:  "Pauses the music player, if it's active.",
			DMPermission: &dmPermission,
		},
		{
			Name:         "ping",
			Description:  "Shows the bot's latency.",
			DMPermission: &dmPermission,
		},
		{
			Name:        "play",
			Description: "Summons the bot and tarts the queue. Additionally, it has the same function as the queue command.",
			Options: []*discordgo.ApplicationCommandOption{
				{
					Type:        discordgo.ApplicationCommandOptionString,
					Name:        "query",
					Description: "Song to search for",
					Required:    false,
				},
			},
			DMPermission: &dmPermission,
		},
		{
			Name:        "prefix",
			Description: "Sets the bot's prefix, or shows the current one if a prefix is omitted.",
			Options: []*discordgo.ApplicationCommandOption{
				{
					Type:        discordgo.ApplicationCommandOptionString,
					Name:        "prefix",
					Description: "New prefix",
					Required:    false,
				},
			},
			DMPermission: &dmPermission,
		},
		{
			Name:        "queue",
			Description: "Queues a song via query or URL. Otherwise, shows the queue. Specify a number to view that page.",
			Options: []*discordgo.ApplicationCommandOption{
				{
					Type:        discordgo.ApplicationCommandOptionString,
					Name:        "query",
					Description: "Song to search for",
					Required:    false,
				},
				{
					Type:        discordgo.ApplicationCommandOptionInteger,
					Name:        "page",
					Description: "Page number to display",
					Required:    false,
				},
			},
			DMPermission: &dmPermission,
		},
		{
			Name:         "removemessages",
			Description:  "Removes any messages by Pas within the last 100 messages in the current channel.",
			DMPermission: &dmPermission,
		},
		{
			Name:         "repeat",
			Description:  "Toggles queue repeating on or off.",
			DMPermission: &dmPermission,
		},
		{
			Name:         "repeatsong",
			Description:  "Toggles song repeating on or off.",
			DMPermission: &dmPermission,
		},
		{
			Name:         "restart",
			Description:  "Restarts the current song.",
			DMPermission: &dmPermission,
		},
		{
			Name:         "resume",
			Description:  "Resumes the music player, if it's paused.",
			DMPermission: &dmPermission,
		},
		{
			Name:         "shuffle",
			Description:  "Randomly shuffles the queue.",
			DMPermission: &dmPermission,
		},
		{
			Name:         "skip",
			Description:  "Skips the currently playing song.",
			DMPermission: &dmPermission,
		},
		{
			Name:        "skipto",
			Description: "Skips everything before the target song. Specify the target song by index.",
			Options: []*discordgo.ApplicationCommandOption{
				{
					Type:        discordgo.ApplicationCommandOptionInteger,
					Name:        "index",
					Description: "Index number to skip to",
					Required:    true,
				},
			},
			DMPermission: &dmPermission,
		},
		{
			Name:        "sysexec",
			Description: "Executes a shell command. Use with extreme caution!",
			Options: []*discordgo.ApplicationCommandOption{
				{
					Type:        discordgo.ApplicationCommandOptionString,
					Name:        "command",
					Description: "Command to run",
					Required:    true,
				},
				{
					Type:        discordgo.ApplicationCommandOptionString,
					Name:        "arguments",
					Description: "Arguments to pass to the command",
					Required:    false,
				},
			},
			DMPermission: &dmPermission,
		},
		{
			Name:        "topqueue",
			Description: "Queues a song via query or URL. Songs are placed at the top of the queue.",
			Options: []*discordgo.ApplicationCommandOption{
				{
					Type:        discordgo.ApplicationCommandOptionString,
					Name:        "query",
					Description: "Song to search for",
					Required:    true,
				},
			},
			DMPermission: &dmPermission,
		},
		{
			Name:        "unqueue",
			Description: "Removes a song from the queue. Specify the song by index.",
			Options: []*discordgo.ApplicationCommandOption{
				{
					Type:        discordgo.ApplicationCommandOptionInteger,
					Name:        "index",
					Description: "Index number to remove",
					Required:    true,
				},
			},
			DMPermission: &dmPermission,
		},
	}

	Commands = map[string]func(ctx core.Context){
		"about":          cmd.AboutCmd,
		"clearqueue":     cmd.ClearQueueCmd,
		"connect":        cmd.ConnectCmd,
		"disconnect":     cmd.DisconnectCmd,
		"evaluate":       cmd.EvalCmd,
		"help":           cmd.HelpCmd,
		"history":        cmd.HistoryCmd,
		"move":           cmd.MoveCmd,
		"nowplaying":     cmd.NowPlayingCmd,
		"pause":          cmd.PauseCmd,
		"ping":           cmd.PingCmd,
		"play":           cmd.PlayCmd,
		"prefix":         cmd.PrefixCmd,
		"queue":          cmd.QueueCmd,
		"repeat":         cmd.RepeatCmd,
		"removemessages": cmd.RemoveMessagesCmd,
		"repeatsong":     cmd.RepeatSongCmd,
		"restart":        cmd.RestartCmd,
		"resume":         cmd.ResumeCmd,
		"shuffle":        cmd.ShuffleCmd,
		"skip":           cmd.SkipCmd,
		"skipto":         cmd.SkipToCmd,
		"sysexec":        cmd.SysExecCmd,
		"topqueue":       cmd.TopQueueCmd,
		"unqueue":        cmd.UnQueueCmd,
	}
)
