// Discord Connection handler and related methods for playing audio
package core

import (
	"fmt"
	"io"
	"time"

	"github.com/bwmarrin/discordgo"
	"github.com/jonas747/dca"
)

type Connection struct {
	VoiceConnection *discordgo.VoiceConnection
}

// Return a new handler with connection data
func NewConnection(conn *discordgo.VoiceConnection) *Connection {
	return &Connection{VoiceConnection: conn}
}

// Plays a DCA encoded stream over the voice connection
func (connection *Connection) Play(ctx *Context, sess *Session) {
	v := connection.VoiceConnection
	// prevents sending on a nil channel
	for !v.Ready {
		time.Sleep(0)
	}

	// this is potentially unnecessary
	err := v.Speaking(true)
	if err != nil {
		ctx.Log.Errorf("error setting speaking: ", err)
	} else {
		defer v.Speaking(false)
	}

	opts := dca.StdEncodeOptions
	opts.RawOutput = true
	opts.Bitrate = 120
	opts.Application = "lowdelay"

	song := sess.Current
	msg := ctx.Reply(0xFFCC66, "💽 Downloading song")
	err = song.Download(ctx)
	if err != nil {
		ctx.Reply(0xBE1931, "❗ Failed to download song")
		ctx.Log.Error("error downloading song: ", err)
		return
	}

	duration := time.Duration(int(time.Second) * song.Duration)
	embed := &discordgo.MessageEmbed{
		Color: 0x3B88C3,
		Fields: []*discordgo.MessageEmbedField{{
			Name:  "🎵 Now Playing",
			Value: fmt.Sprintf("[%s](%s)", song.Title, song.WebPage),
		}},
		Thumbnail: &discordgo.MessageEmbedThumbnail{
			URL: song.Thumbnail,
		},
		Footer: &discordgo.MessageEmbedFooter{
			Text: fmt.Sprintf("Duration: %s | Requester: %s", duration.String(), song.Requester),
		},
	}
	ctx.Edit(msg, embed)

	encodeSession, err := dca.EncodeFile(fmt.Sprintf("%s.opus", song.Path), opts)
	if err != nil {
		ctx.Reply(0xBE1931, "❗ Failed to create player")
		ctx.Log.Error("error creating encode session: ", err)
		return
	}
	done := make(chan error)
	stream := dca.NewStream(encodeSession, v, done)
	queue := ctx.Queues.GetQueue(ctx.Guild.ID)
	queue.Stream = stream

wait:
	for {
		select {
		case err := <-done:
			if err != nil && err != io.EOF {
				ctx.Reply(0xBE1931, "❗ Stream ended unexpectedly")
			}
			queue.Stream = nil
			queue.Current = nil
			encodeSession.Cleanup()
			break wait
		case <-queue.Stop:
			queue.Stream = nil
			queue.Current = nil
			encodeSession.Cleanup()
			break wait
		}
	}
}
