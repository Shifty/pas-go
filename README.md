<div align="left">
  <a href="pas-img">
    <img src="https://i.imgur.com/QM3EPsa.png" alt="Pas Icon" height="64" width="64" align="left">
  </a>
</div>

# Personal Audio Streamer

### A music bot for Discord

> This is a Go rewrite of my original [Pas](https://gitlab.com/Shifty/pas) application.

### Config

You can create a Discord Application [here](https://discord.com/developers/applications/)

Create the file `static/config.json` and add the following values to it:

| Key | Description | Type | 
| :---: | ----------- | :---: |
| `debug` | Sets the logging level to debug and prints errors | **bool** |
| `slash` | Whether or not slash commands should be eneabled | **bool** |
| `prefix` | The default prefix the bot will respond to | **string** |
| `token` | Your Discord Application's token | **string** |
| `owners` | A list of user IDs to be marked as the bot's owners | **list[string]** |
| `status` | The status to be set on the bot's profile | **string** |
| `status_type` | 1 for playing, 2 for listening, omit for no status | **int** |
| `error_channel` | The ID of the channel to log errors in | **string** |

For YouTube cookies, use
[Get cookies.txt](https://chrome.google.com/webstore/detail/get-cookiestxt/bgaddhkoddajcdgocldbbfleckgcbcid/) (for Chrome) or
[cookies.txt](https://addons.mozilla.org/en-US/firefox/addon/cookies-txt/) (for Firefox)
and put the file in `static/`. It should be named `cookies.txt`.

### Setup
```shell
$ go build -o pas.exe
$ ./pas.exe
```
