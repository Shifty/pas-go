// YouTube handler and related methods for interacting with YouTubeDL
package core

import (
	"bytes"
	"encoding/json"
	"os"
	"os/exec"
	"strings"
)

const (
	ErrorType    = -1
	VideoType    = 0
	PlaylistType = 1
)

type (
	YouTube struct {
	}
	Playlist struct {
		Entries []*Video
	}
	Video struct {
		ID        string  `json:"id"`
		Title     string  `json:"title"`
		Thumbnail string  `json:"thumbnail"`
		WebPage   string  `json:"webpage_url"`
		Duration  float32 `json:"duration"`
	}
)

// Create a cache directory if it doesn't already exists
func init() {
	if _, err := os.Stat("cache"); !os.IsNotExist(err) {
		err = os.RemoveAll("cache")
		if err != nil {
			Log.Fatal(err)
		}
		err = os.Mkdir("cache", os.ModeDir)
		if err != nil {
			Log.Fatal(err)
		}
	}
}

// Searches YouTubeDL with the given query
func (youtube YouTube) getResult(query string) (*bytes.Buffer, error) {
	if len(strings.Fields(query)) == 1 && strings.HasPrefix(query, "http") {
		query = strings.Split(query, "&")[0]
	}
	parameters := infoParams(query)
	cmd := exec.Command("yt-dlp", parameters...)
	var output bytes.Buffer
	cmd.Stdout = &output
	err := cmd.Run()
	if err != nil {
		return nil, err
	}
	return &output, err
}

// Searches YouTubeDL and returns a playlist object
func (youtube YouTube) GetPlaylist(query string) (*Playlist, error) {
	output, err := youtube.getResult(query)
	if err != nil {
		return nil, err
	}
	var playlist Playlist
	err = json.Unmarshal(output.Bytes(), &playlist)
	if err != nil {
		return nil, err
	}
	return &playlist, err
}

// Searches YouTubeDL and returns a video object
func (youtube YouTube) GetVideo(query string) (*Video, error) {
	output, err := youtube.getResult(query)
	if err != nil {
		return nil, err
	}
	var video Video
	err = json.Unmarshal(output.Bytes(), &video)
	if err != nil {
		return nil, err
	}
	return &video, err
}

// Returns YouTubeDL params for extracting data
func infoParams(query string) []string {
	params := []string{
		"--source-address", "0.0.0.0",
		"--default-search", "auto",
		"--no-check-certificate",
		"--restrict-filenames",
		"--ignore-errors",
		"--no-playlist",
		"--no-warnings",
		"--dump-json",
		"--quiet",
		query,
	}
	if _, err := os.Stat("static/cookies.txt"); !os.IsNotExist(err) {
		params = append(params, []string{"--cookies", "static/cookies.txt"}...)
	}
	return params
}
