package cmd

import (
	"main/core"
)

func PlayCmd(ctx core.Context) {
	userVC, err := ctx.GetVoiceChannel(ctx.Author.ID)
	if err != nil {
		ctx.Reply(0xBE1931, "❗ Failed to retrieve voice channel")
		ctx.Errors.ErrChan <- ctx.Errors.Error(&ctx, err)
		return
	}
	if userVC == nil {
		ctx.Reply(0xBE1931, "❗ You are not in a voice channel")
		return
	}
	botVC, err := ctx.GetVoiceChannel(ctx.Session.State.User.ID)
	if err != nil {
		ctx.Reply(0xBE1931, "❗ Failed to retrieve voice channel")
		ctx.Errors.ErrChan <- ctx.Errors.Error(&ctx, err)
		return
	}
	sess := ctx.Sessions.GetSession(ctx.Guild.ID)
	if botVC == nil || sess == nil {
		command, _ := ctx.Commands.Get("connect")
		c := command.Function
		c(ctx) // TODO abort if this command errors
	} else if botVC.ID != userVC.ID {
		ctx.Reply(0xBE1931, "❗ We are not in the same channel")
		return
	}
	if botVC, err = ctx.GetVoiceChannel(ctx.Session.State.User.ID); botVC == nil {
		ctx.Errors.ErrChan <- ctx.Errors.Error(&ctx, err)
		return
	}

	queue := ctx.Queues.GetQueue(ctx.Guild.ID)

	if _, f := ctx.Options["query"]; f {
		command, _ := ctx.Commands.Get("queue")
		c := command.Function
		c(ctx) // TODO abort if this command errors

		if queue.Empty() {
			return
		}
		if stream := queue.Stream; stream != nil && !stream.Paused() {
			return
		}
	}

	stream := queue.Stream
	if stream != nil && stream.Paused() {
		queue.Stream.SetPaused(false)
		ctx.Reply(0x3B88C3, "⏸ Music resumed")
		return
	}
	if stream != nil && !stream.Paused() {
		ctx.Reply(0xBE1931, "❗ The player is not paused")
		return
	}
	if queue.Empty() {
		ctx.Reply(0xBE1931, "❗ There's nothing to play")
		return
	}
	go queue.Start(&ctx)
}
