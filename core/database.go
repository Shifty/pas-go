// Database handler and related methods for interacting with the configured MongoDB database
package core

import (
	"context"
	"fmt"
	"os"
	"os/signal"
	"syscall"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

var sc chan bool // Used to signal for a disconnect on ctrl+c

type (
	// Wrapper for MongoClient so I can use a database
	// more directly without directly passing it around
	Database struct {
		cfg    *Config
		uri    string
		name   string
		client *mongo.Client
		DB     *mongo.Database
	}
)

// Returns an instance of the configured MongoDB address
func NewDatabaseHandler(cfg *Config) *Database {
	var uri string
	if cfg.DB.Auth {
		uri = fmt.Sprintf("mongodb://%s:%s@%s:%s", cfg.DB.User, cfg.DB.Pass, cfg.DB.Host, cfg.DB.Port)
	} else {
		uri = fmt.Sprintf("mongodb://%s:%s", cfg.DB.Host, cfg.DB.Port)
	}
	db := &Database{cfg: cfg, uri: uri, name: cfg.DB.Name}
	db.Connect(cfg)
	return db
}

// Connects the the stored MongoDB database
func (db *Database) Connect(ctx *Config) {

	client, err := mongo.Connect(context.TODO(), options.Client().ApplyURI(db.uri))
	if err != nil {
		Log.Fatal("failed to connect to database")
	}
	Log.Info("connected to database")

	db.client = client
	db.DB = client.Database(db.name)
	go stopHandler(db.client)
}

type Settings struct {
	GuildID string `json:"guildID"`
	Prefix  string `json:"prefix"`
}

// Retreives settings for the specified guild
func (db *Database) GetSettings(guildID string) *Settings {
	settings := Settings{}
	query := bson.D{{Key: "guildID", Value: guildID}}
	doc := db.DB.Collection("settings").FindOne(context.TODO(), query)
	err := doc.Decode(&settings)
	if err == mongo.ErrNoDocuments {
		settings.GuildID = guildID
		settings.Prefix = db.cfg.Prefix
	}
	return &settings
}

// Updates settings for the specified guild
func (db *Database) SetSetting(guildID string, settings *Settings) {
	query := bson.D{{Key: "guildID", Value: guildID}}
	doc := db.DB.Collection("settings").FindOne(context.TODO(), query)
	if doc.Err() != mongo.ErrNoDocuments {
		update := bson.D{{Key: "$set", Value: bson.D{
			{Key: "guildID", Value: settings.GuildID},
			{Key: "prefix", Value: settings.Prefix},
		}},
		}
		db.DB.Collection("settings").UpdateOne(context.TODO(), query, update)
	} else {
		document := bson.D{
			{Key: "guildID", Value: settings.GuildID},
			{Key: "prefix", Value: db.cfg.Prefix},
		}
		db.DB.Collection("settings").InsertOne(context.TODO(), document)
	}
}

// Handles keyboard interupts. Aborts if received
func stopHandler(mc *mongo.Client) {
	c := make(chan os.Signal, 1)
	sc = make(chan bool)
	signal.Notify(c, os.Interrupt, syscall.SIGTERM)
	go func() {
		<-c // wait for interupt
		Log.Info("received interupt. stopping...")
		sc <- true // send value to disconnect
		<-sc       // wait until finished
		os.Exit(0)
	}()
	go func() {
		<-sc // wait for value
		mc.Disconnect(context.TODO())
		sc <- true // notify finished
	}()
}
