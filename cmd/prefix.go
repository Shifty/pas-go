package cmd

import (
	"fmt"
	"main/core"
	"strings"

	"github.com/bwmarrin/discordgo"
)

func PrefixCmd(ctx core.Context) {
	settings := ctx.Database.GetSettings(ctx.Guild.ID)
	if prefix, f := ctx.Options["prefix"].(string); f {
		perm := discordgo.PermissionManageServer
		hasPerm, _ := ctx.HasPerm(ctx.TextChannel.ID, ctx.Session.State.User.ID, perm)
		if !hasPerm {
			ctx.Reply(0xBE1931, "⛔ Manage Server required")

		}
		if strings.Contains(prefix, "\n") {
			ctx.Reply(0xBE1931, "❗ Prefix can't contain line breaks")
			return
		}

		settings.Prefix = prefix
		ctx.Database.SetSetting(ctx.Guild.ID, settings)
		ctx.Reply(0x77b255, fmt.Sprintf("✅ Prefix set to `%s`", settings.Prefix))
		return
	}

	ctx.Reply(0x3b88c3, fmt.Sprintf("ℹ️ The prefix is `%s`", settings.Prefix))
}
