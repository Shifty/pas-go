package cmd

import (
	"main/core"
	"strconv"
	"time"

	"github.com/bwmarrin/discordgo"
)

func RemoveMessagesCmd(ctx core.Context) {
	messages, err := ctx.Session.ChannelMessages(ctx.TextChannel.ID, 100, "", "", "")
	if err != nil {
		ctx.Reply(0xBE1931, "❗ Failed to retrieve messages")
		ctx.Errors.ErrChan <- ctx.Errors.Error(&ctx, err)
		return
	}

	minTime := int((time.Now().Unix()-14*24*60*60)*1000.0-1420070400000) << 22 // 14 days ago
	bulkDelete := []string{}
	singleDelete := []string{}
	for _, msg := range messages {
		if msg.Author.ID == ctx.Session.State.User.ID {
			if ts, _ := strconv.Atoi(msg.ID); ts < minTime {
				singleDelete = append(singleDelete, msg.ID)
			} else {
				bulkDelete = append(bulkDelete, msg.ID)
			}
		}
	}

	perm := discordgo.PermissionManageMessages
	hasPerm, _ := ctx.HasPerm(ctx.TextChannel.ID, ctx.Session.State.User.ID, perm)
	if hasPerm {
		if len(bulkDelete) == 1 {
			singleDelete = append(singleDelete, bulkDelete...)
		}
		if len(bulkDelete) > 0 {
			err := ctx.Session.ChannelMessagesBulkDelete(ctx.TextChannel.ID, bulkDelete)
			if err != nil {
				ctx.Reply(0xBE1931, "❗ There was an error while removing messages")
				ctx.Errors.ErrChan <- ctx.Errors.Error(&ctx, err)
				return
			}
		}
		if len(singleDelete) > 0 {
			for _, msgID := range singleDelete {
				ctx.Session.ChannelMessageDelete(ctx.TextChannel.ID, msgID)
			}
		}
	} else {
		singleDelete = append(singleDelete, bulkDelete...)
		for _, msgID := range singleDelete {
			ctx.Session.ChannelMessageDelete(ctx.TextChannel.ID, msgID)
		}
	}
	ctx.Reply(0x77b255, "✅ Removed messages")
}
