package cmd

import (
	"fmt"
	"main/core"
	"strings"
	"time"

	"github.com/bwmarrin/discordgo"
)

func TopQueueCmd(ctx core.Context) {
	userVC, err := ctx.GetVoiceChannel(ctx.Author.ID)
	if err != nil {
		ctx.Reply(0xBE1931, "❗ Failed to retrieve voice channel")
		ctx.Errors.ErrChan <- ctx.Errors.Error(&ctx, err)
		return
	}
	if userVC == nil {
		ctx.Reply(0xBE1931, "❗ You are not in a voice channel")
		return
	}
	botVC, err := ctx.GetVoiceChannel(ctx.Session.State.User.ID)
	if err != nil {
		ctx.Reply(0xBE1931, "❗ Failed to retrieve voice channel")
		ctx.Errors.ErrChan <- ctx.Errors.Error(&ctx, err)
		return
	}
	if botVC == nil {
		ctx.Reply(0xBE1931, "❗ I am not in a voice channel")
		return
	}
	if botVC.ID != userVC.ID {
		ctx.Reply(0xBE1931, "❗ We are not in the same channel")
		return
	}

	var f bool
	var query string
	if query, f = ctx.Options["query"].(string); !f {
		ctx.Reply(0xBE1931, "❗ Missing input")
		return
	}

	var msg *discordgo.Message
	queue := ctx.Queues.GetQueue(ctx.Guild.ID)

	if strings.Contains(query, "/watch?") || !strings.Contains(query, "http") {
		msg = ctx.Reply(0xFFCC66, "💽 Queueing song...")

		video, err := ctx.YouTube.GetVideo(query)
		if err != nil {
			ctx.Reply(0xBE1931, "❗ Failed to retrieve video")
			return
		}
		if video == nil {
			ctx.Edit(msg, 0x696969, "🔍 No results")
			return
		}
		song := core.NewSong(video, ctx.Author.Username)
		queue.Insert(0, *song)

		duration := time.Duration(int(time.Second) * song.Duration)
		embed := &discordgo.MessageEmbed{
			Color: 0x66CC66,
			Fields: []*discordgo.MessageEmbedField{{
				Name:  "✅ Top-Queued song",
				Value: fmt.Sprintf("[%s](%s)", song.Title, song.WebPage),
			}},
			Thumbnail: &discordgo.MessageEmbedThumbnail{URL: song.Thumbnail},
			Footer:    &discordgo.MessageEmbedFooter{Text: "Duration: " + duration.String()},
		}
		ctx.Edit(msg, embed)

	} else if strings.Contains(query, "/playlist?") {
		msg = ctx.Reply(0xFFCC66, "💽 Queueing playlist...")

		playlist, err := ctx.YouTube.GetPlaylist(query)
		if err != nil {
			ctx.Edit(msg, 0xBE1931, "❗ Failed to retrieve playlist")
			return
		}
		if playlist == nil {
			ctx.Edit(msg, 0x696969, "🔍 No results")
			return
		}
		pSongs := make([]core.Song, len(playlist.Entries))
		for i, v := range playlist.Entries {
			pSongs[i] = *core.NewSong(v, ctx.Author.Username)
		}

		queue.InsertN(0, pSongs)
		ctx.Edit(msg, 0x66CC66, fmt.Sprintf("✅ Top-Queued %d songs", len(playlist.Entries)))

	} else {
		ctx.Reply(0xBE1931, "❗ Could not parse input")
	}
}
