// Session handler and related methods for interacting with discord voice sessions
package core

import (
	"sync"

	"github.com/bwmarrin/discordgo"
)

var Sessions map[string]*Session
var sessLocks = make(map[string]*sync.Mutex)

type (
	SessionHandler struct {
		sessions map[string]*Session
	}
	Session struct {
		guildID    string
		Current    *Song
		Connection *Connection
		Closing    chan bool
	}
)

// Returns a new handler for holding session data
func NewSessionHandler() *SessionHandler {
	return &SessionHandler{make(map[string]*Session)}
}

// Creates an new session and assigns it to a guild
func (handler *SessionHandler) newSession(guildID string, connection *Connection) *Session {
	sess := &Session{
		guildID:    guildID,
		Connection: connection,
		Closing:    make(chan bool, 1),
	}
	handler.sessions[guildID] = sess
	return sess
}

// Gets a guild's session, or creates one if it doesn't exist.
func (handler *SessionHandler) GetSession(guildID string) *Session {
	return handler.sessions[guildID]
}

// Disconnects from a voice channel
func (handler *SessionHandler) Leave(discord *discordgo.Session, session *Session) error {
	lock := GetLock(session.guildID)
	lock.Lock()
	defer lock.Unlock()

	err := session.Connection.VoiceConnection.Disconnect()
	delete(handler.sessions, session.guildID)
	return err
}

// Connects to a voice channel
func (handler *SessionHandler) Join(session *discordgo.Session, guildID, channelID string) error {
	lock := GetLock(guildID)
	lock.Lock()
	defer lock.Unlock()

	vc, err := session.ChannelVoiceJoin(guildID, channelID, false, true)
	if err != nil {
		return err
	}

	sess := handler.GetSession(guildID)
	if sess == nil {
		handler.newSession(guildID, NewConnection(vc))
	} else {
		sess.Connection = NewConnection(vc)
	}
	return nil
}

// TODO this is inelegant at best
// Map of guild-bound mutexes to prevent modifying the
// voice state before a previous change has finished
func GetLock(guildID string) *sync.Mutex {
	lock, f := sessLocks[guildID]
	if !f {
		lock = &sync.Mutex{}
		sessLocks[guildID] = lock
	}
	return lock
}
