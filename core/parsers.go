// Parser functions/map for parsing legacy and slash arguments(legacy)/options(slash) into one universal map
package core

import (
	"strconv"
	"strings"

	"github.com/bwmarrin/discordgo"
)

// Legacy (message based) and slash commands have different models for arguments/options.
// These parsers standardize how arguments/options are provided; one for each command.

var (
	Parsers = map[string]*Options{
		"about":          {Parser: AboutParser},
		"clearqueue":     {Parser: ClearQueueParser},
		"connect":        {Parser: ConnectParser},
		"disconnect":     {Parser: DisconnectParser},
		"evaluate":       {Parser: EvaluateParser},
		"help":           {Parser: HelpParser},
		"history":        {Parser: HistoryParser},
		"move":           {Parser: MoveParser},
		"nowplaying":     {Parser: NowPlayingParser},
		"pause":          {Parser: PauseParser},
		"ping":           {Parser: PingParser},
		"play":           {Parser: PlayParser},
		"prefix":         {Parser: PrefixParser},
		"queue":          {Parser: QueueParser},
		"removemessages": {Parser: RemoveMessagesParser},
		"repeat":         {Parser: RepeatParser},
		"repeatsong":     {Parser: RepeatSongParser},
		"restart":        {Parser: RestartParser},
		"resume":         {Parser: ResumeParser},
		"shuffle":        {Parser: ShuffleParser},
		"skip":           {Parser: SkipParser},
		"skipto":         {Parser: SkipToParser},
		"sysexec":        {Parser: SysExecParser},
		"topqueue":       {Parser: TopQueueParser},
		"unqueue":        {Parser: UnQueueParser},
	}
)

// Container to hold an ambiguous parser function
type Options struct {
	Parser func(*Context, map[string]*discordgo.ApplicationCommandInteractionDataOption, []string) map[string]interface{}
}

func AboutParser(ctx *Context, opt map[string]*discordgo.ApplicationCommandInteractionDataOption, args []string) map[string]interface{} {
	optMap := make(map[string]interface{})
	return optMap
}

func ClearQueueParser(ctx *Context, opt map[string]*discordgo.ApplicationCommandInteractionDataOption, args []string) map[string]interface{} {
	optMap := make(map[string]interface{})
	return optMap
}

func ConnectParser(ctx *Context, opt map[string]*discordgo.ApplicationCommandInteractionDataOption, args []string) map[string]interface{} {
	optMap := make(map[string]interface{})
	return optMap
}

func DisconnectParser(ctx *Context, opt map[string]*discordgo.ApplicationCommandInteractionDataOption, args []string) map[string]interface{} {
	optMap := make(map[string]interface{})
	return optMap
}

func EvaluateParser(ctx *Context, opt map[string]*discordgo.ApplicationCommandInteractionDataOption, args []string) map[string]interface{} {
	optMap := make(map[string]interface{})
	if ctx.isSlash {
		if opt["code"] != nil {
			optMap["code"] = opt["code"].StringValue()
		}
	} else {
		if len(args) >= 1 {
			optMap["code"] = strings.Join(args, " ")
		}
	}
	return optMap
}

func HelpParser(ctx *Context, opt map[string]*discordgo.ApplicationCommandInteractionDataOption, args []string) map[string]interface{} {
	optMap := make(map[string]interface{})
	if ctx.isSlash {
		if opt["query"] != nil {
			optMap["query"] = opt["query"].StringValue()
		}
	} else {
		if len(args) >= 1 {
			optMap["query"] = args[0]
		}
	}
	return optMap
}

func HistoryParser(ctx *Context, opt map[string]*discordgo.ApplicationCommandInteractionDataOption, args []string) map[string]interface{} {
	optMap := make(map[string]interface{})
	return optMap
}

func MoveParser(ctx *Context, opt map[string]*discordgo.ApplicationCommandInteractionDataOption, args []string) map[string]interface{} {
	optMap := make(map[string]interface{})
	if ctx.isSlash {
		if opt["current-position"] != nil {
			optMap["current-position"] = int(opt["current-position"].IntValue())
		}
		if opt["target-position"] != nil {
			optMap["target-position"] = int(opt["target-position"].IntValue())
		}
	} else {
		if len(args) >= 2 {
			if i, err := strconv.Atoi(args[0]); err == nil {
				optMap["current-position"] = i
			}
			if i, err := strconv.Atoi(args[1]); err == nil {
				optMap["target-position"] = i
			}
		}
	}
	return optMap
}

func NowPlayingParser(ctx *Context, opt map[string]*discordgo.ApplicationCommandInteractionDataOption, args []string) map[string]interface{} {
	optMap := make(map[string]interface{})
	return optMap
}

func PauseParser(ctx *Context, opt map[string]*discordgo.ApplicationCommandInteractionDataOption, args []string) map[string]interface{} {
	optMap := make(map[string]interface{})
	return optMap
}

func PingParser(ctx *Context, opt map[string]*discordgo.ApplicationCommandInteractionDataOption, args []string) map[string]interface{} {
	optMap := make(map[string]interface{})
	return optMap
}

func PlayParser(ctx *Context, opt map[string]*discordgo.ApplicationCommandInteractionDataOption, args []string) map[string]interface{} {
	optMap := make(map[string]interface{})
	if ctx.isSlash {
		if opt["query"] != nil {
			optMap["query"] = opt["query"].StringValue()
		}
	} else {
		if len(args) >= 1 {
			optMap["query"] = strings.Join(args, " ")
		}
	}
	return optMap
}

func PrefixParser(ctx *Context, opt map[string]*discordgo.ApplicationCommandInteractionDataOption, args []string) map[string]interface{} {
	optMap := make(map[string]interface{})
	if ctx.isSlash {
		if opt["prefix"] != nil {
			optMap["prefix"] = opt["prefix"].StringValue()
		}
	} else {
		if len(args) > 0 {
			optMap["prefix"] = args[0]
		}
	}
	return optMap
}

func QueueParser(ctx *Context, opt map[string]*discordgo.ApplicationCommandInteractionDataOption, args []string) map[string]interface{} {
	optMap := make(map[string]interface{})
	if ctx.isSlash {
		if opt["query"] != nil {
			optMap["query"] = opt["query"].StringValue()
		}
		if opt["page"] != nil {
			optMap["page"] = int(opt["page"].IntValue())
		}
	} else {
		if len(args) == 1 {
			if pg, err := strconv.Atoi(args[0]); err == nil {
				optMap["page"] = pg
			} else {
				optMap["query"] = strings.Join(args, " ")
			}
		} else if len(args) > 1 {
			optMap["query"] = strings.Join(args, " ")
		}
	}
	return optMap
}

func RemoveMessagesParser(ctx *Context, opt map[string]*discordgo.ApplicationCommandInteractionDataOption, args []string) map[string]interface{} {
	optMap := make(map[string]interface{})
	return optMap
}

func RepeatParser(ctx *Context, opt map[string]*discordgo.ApplicationCommandInteractionDataOption, args []string) map[string]interface{} {
	optMap := make(map[string]interface{})
	return optMap
}

func RepeatSongParser(ctx *Context, opt map[string]*discordgo.ApplicationCommandInteractionDataOption, args []string) map[string]interface{} {
	optMap := make(map[string]interface{})
	return optMap
}

func RestartParser(ctx *Context, opt map[string]*discordgo.ApplicationCommandInteractionDataOption, args []string) map[string]interface{} {
	optMap := make(map[string]interface{})
	return optMap
}

func ResumeParser(ctx *Context, opt map[string]*discordgo.ApplicationCommandInteractionDataOption, args []string) map[string]interface{} {
	optMap := make(map[string]interface{})
	return optMap
}

func ShuffleParser(ctx *Context, opt map[string]*discordgo.ApplicationCommandInteractionDataOption, args []string) map[string]interface{} {
	optMap := make(map[string]interface{})
	return optMap
}

func SkipParser(ctx *Context, opt map[string]*discordgo.ApplicationCommandInteractionDataOption, args []string) map[string]interface{} {
	optMap := make(map[string]interface{})
	return optMap
}

func SkipToParser(ctx *Context, opt map[string]*discordgo.ApplicationCommandInteractionDataOption, args []string) map[string]interface{} {
	optMap := make(map[string]interface{})
	if ctx.isSlash {
		if opt["index"] != nil {
			optMap["index"] = int(opt["index"].IntValue())
		}
	} else {
		if len(args) >= 1 {
			if i, err := strconv.Atoi(args[0]); err == nil {
				optMap["index"] = i
			}
		}
	}
	return optMap
}

func SysExecParser(ctx *Context, opt map[string]*discordgo.ApplicationCommandInteractionDataOption, args []string) map[string]interface{} {
	optMap := make(map[string]interface{})
	if ctx.isSlash {
		if opt["command"] != nil {
			optMap["command"] = opt["command"].StringValue()
		}
		if opt["arguments"] != nil {
			optMap["arguments"] = strings.Split(opt["arguments"].StringValue(), " ")
		}
	} else {
		if len(args) > 0 {
			optMap["command"] = args[0]
			if len(args) > 1 {
				optMap["arguments"] = args[1:]
			}
		}
	}
	return optMap
}

func TopQueueParser(ctx *Context, opt map[string]*discordgo.ApplicationCommandInteractionDataOption, args []string) map[string]interface{} {
	optMap := make(map[string]interface{})
	if ctx.isSlash {
		if opt["query"] != nil {
			optMap["query"] = opt["query"].StringValue()
		}
	} else {
		if len(args) >= 1 {
			optMap["query"] = strings.Join(args, " ")
		}
	}
	return optMap
}

func UnQueueParser(ctx *Context, opt map[string]*discordgo.ApplicationCommandInteractionDataOption, args []string) map[string]interface{} {
	optMap := make(map[string]interface{})
	if ctx.isSlash {
		if opt["index"] != nil {
			optMap["index"] = int(opt["index"].IntValue())
		}
	} else {
		if len(args) >= 1 {
			if i, err := strconv.Atoi(args[0]); err == nil {
				optMap["index"] = i
			}
		}
	}
	return optMap
}
