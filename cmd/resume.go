package cmd

import "main/core"

func ResumeCmd(ctx core.Context) {
	userVC, err := ctx.GetVoiceChannel(ctx.Author.ID)
	if err != nil {
		ctx.Reply(0xBE1931, "❗ Failed to retrieve voice channel")
		ctx.Errors.ErrChan <- ctx.Errors.Error(&ctx, err)
		return
	}
	if userVC == nil {
		ctx.Reply(0xBE1931, "❗ You are not in a voice channel")
		return
	}
	botVC, err := ctx.GetVoiceChannel(ctx.Session.State.User.ID)
	if err != nil {
		ctx.Reply(0xBE1931, "❗ Failed to retrieve voice channel")
		ctx.Errors.ErrChan <- ctx.Errors.Error(&ctx, err)
		return
	}
	if botVC == nil {
		ctx.Reply(0xBE1931, "❗ I am not in a voice channel")
		return
	}
	if botVC.ID != userVC.ID {
		ctx.Reply(0xBE1931, "❗ We are not in the same channel")
		return
	}

	queue := ctx.Queues.GetQueue(ctx.Guild.ID)
	empty := queue.Empty()
	queue.Lock.Lock()
	defer queue.Lock.Unlock()

	if stream := queue.Stream; stream != nil {
		if stream.Paused() {
			queue.Stream.SetPaused(false)
			ctx.Reply(0x3B88C3, "⏸ Music resumed")
		} else {
			ctx.Reply(0xBE1931, "❗ The player is not paused")
		}
	} else {
		if !empty {
			sess := ctx.Sessions.GetSession(ctx.Guild.ID)
			if sess == nil {
				command, _ := ctx.Commands.Get("join")
				c := command.Function
				c(ctx) // TODO abort if this command errors
			}
			go queue.Start(&ctx)
		} else {
			ctx.Reply(0xBE1931, "❗ There's nothing to play")
		}
	}
}
