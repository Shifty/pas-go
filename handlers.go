// Discord websocket handler functions
package main

import (
	"main/core"
	"strings"

	"github.com/bwmarrin/discordgo"
)

// Converts slash command options into a basic map
func makeOptionMap(options []*discordgo.ApplicationCommandInteractionDataOption) map[string]*discordgo.ApplicationCommandInteractionDataOption {
	optionMap := make(map[string]*discordgo.ApplicationCommandInteractionDataOption, len(options))
	for _, opt := range options {
		optionMap[opt.Name] = opt
	}
	return optionMap
}

// Sent on the Discord websocket InteractionCreate event
func slashHandler(session *discordgo.Session, data *discordgo.InteractionCreate) {
	if !booted {
		return
	}

	if c, ok := Commands[data.ApplicationCommandData().Name]; ok {

		channel, err := session.State.Channel(data.ChannelID)
		if err != nil {
			channel, err = session.Channel(data.ChannelID)
			if err != nil {
				// core.Log.Error("failed to get channel: ", err)
				return
			}
		}

		guild, err := session.State.Guild(data.GuildID)
		if err != nil {
			guild, err = session.Guild(data.GuildID)
			if err != nil {
				// core.Log.Error("failed to get guild: ", err)
				return
			}
		}

		ctx := core.NewContext(
			session,
			data,
			guild,
			channel,
			nil,
			data.Member.User,
			static,
			nil,
			commands,
			errors,
			sessions,
			queues,
			youtube,
			database,
			true,
		)

		opt := core.Parsers[data.ApplicationCommandData().Name]
		ctx.Options = opt.Parser(ctx, makeOptionMap(data.ApplicationCommandData().Options), nil)

		core.LogCommandUsage(ctx)

		go c(*ctx)
	}
}

// Sent on the Discord websocket MessageCreate event
func messageCreate(session *discordgo.Session, message *discordgo.MessageCreate) {
	if !booted {
		return
	}
	if message.Author.Bot {
		return
	}

	settings := database.GetSettings(message.GuildID)
	prefix := settings.Prefix // guild-bound prefix
	content := message.Content

	if len(content) <= len(prefix) {
		return
	}
	if content[:len(prefix)] != prefix {
		return
	}
	if len(content[len(prefix):]) < 1 {
		return
	}

	args := strings.Fields(content)
	name := strings.ToLower(args[0][len(prefix):])
	args = args[1:]
	command, found := commands.Get(name)
	if !found {
		return
	}

	// For whatever reason, this fails when the channel is a DM
	channel, err := session.State.Channel(message.ChannelID)
	if err != nil {
		channel, err = session.Channel(message.ChannelID)
		if err != nil {
			// core.Log.Error("failed to get channel: ", err)
			return
		}
	}

	// This obviously fails when the channel is a DM,
	// but without a channel object, this check is pointless.
	guild, err := session.State.Guild(message.GuildID)
	if err != nil && channel.Type == discordgo.ChannelTypeGuildText {
		guild, err = session.Guild(message.GuildID)
		if err != nil {
			// core.Log.Error("failed to get guild: ", err)
			return
		}
	}

	perms, err := session.State.UserChannelPermissions(session.State.User.ID, channel.ID)
	if err == nil {
		if perms&int64(discordgo.PermissionSendMessages) == 0 {
			session.MessageReactionAdd(message.ChannelID, message.ID, "❗")
			return
		}
		if perms&int64(discordgo.PermissionEmbedLinks) == 0 {
			errText := "❗ Missing `Embed Links` permission."
			errText += " Commands will not work without this."
			session.ChannelMessageSend(channel.ID, errText)
			return
		}
	}

	ctx := core.NewContext(
		session,
		nil,
		guild,
		channel,
		message,
		message.Author,
		static,
		command,
		commands,
		errors,
		sessions,
		queues,
		youtube,
		database,
		false,
	)

	opt := core.Parsers[command.Name]
	o := opt.Parser(ctx, nil, args)
	ctx.Options = o

	core.LogCommandUsage(ctx)

	c := command.Function
	go c(*ctx)
}

// Sent on the VoiceStateUpdate Discord websocket event
func voiceStateUpdate(session *discordgo.Session, vc *discordgo.VoiceStateUpdate) {
	// Disconnects if the voice connection is lost.
	// This happens if the bot is manually disconnected/moved.
	if vc.UserID == session.State.User.ID {
		if _, f := session.VoiceConnections[vc.GuildID]; !f {
			lock := core.GetLock(vc.GuildID)
			lock.Lock() // don't reconnect until we're done
			defer lock.Unlock()

			queue := queues.GetQueue(vc.GuildID)
			if queue != nil && queue.Stream != nil {
				queue.Stop <- true // prevent DCA stream error
			}
			sess := sessions.GetSession(vc.GuildID)
			if sess != nil && sess.Connection != nil {
				sess.Connection.VoiceConnection.Disconnect()
			}
		}
	}
}

// Sent on the Discord websocket Ready event
func ready(session *discordgo.Session, event *discordgo.Ready) {
	u := session.State.User
	core.Log.Infof("logged in as %v#%v", u.Username, u.Discriminator)
	readyChan <- true
}
