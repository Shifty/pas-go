// Logger initialization for starting the project logger
package core

import (
	"fmt"
	"log"
	"os"
	"strings"
	"time"

	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

var Log *zap.SugaredLogger
var Atom zap.AtomicLevel

// Sets the global log variable
func init() {
	Log = initLogger()
}

// Initialized the project logger
func initLogger() *zap.SugaredLogger {
	if _, err := os.Stat("logs"); os.IsNotExist(err) {
		os.Mkdir("logs", os.ModePerm)
	}
	year, month, day := time.Now().Date()
	fp := fmt.Sprintf("logs/pas.%d-%d-%d.log", year, month, day)
	file, err := os.OpenFile(fp, os.O_APPEND|os.O_WRONLY|os.O_CREATE, 0600)
	if err != nil {
		log.Fatal("error creating log file")
	}

	Atom = zap.NewAtomicLevel()
	cfg := zap.NewProductionEncoderConfig()
	cfg.EncodeTime = zapcore.TimeEncoderOfLayout("2006-01-02 15:04:05")
	encoder := zapcore.NewConsoleEncoder(cfg)
	core := zapcore.NewTee(
		zapcore.NewCore(encoder, zapcore.AddSync(file), Atom),
		zapcore.NewCore(encoder, zapcore.AddSync(os.Stdout), Atom),
	)

	logger := zap.New(core)
	defer logger.Sync()
	return logger.Sugar()
}

// Logs command usage data
func LogCommandUsage(ctx *Context) {
	logFormat := "%s | SRV: %s [%s] | CHN: %s [%s] | USR: %s#%s [%s]"
	var name string
	if ctx.isSlash {
		name = ctx.Data.ApplicationCommandData().Name
	} else {
		name = ctx.Command.Name
	}
	logArgs := []interface{}{
		strings.ToUpper(name),
		ctx.Guild.Name,
		ctx.Guild.ID,
		ctx.TextChannel.Name,
		ctx.TextChannel.ID,
		ctx.Author.Username,
		ctx.Author.Discriminator,
		ctx.Author.ID,
	}

	args := []string{}
	for _, v := range ctx.Options {
		switch arg := v.(type) {
		case []string:
			args = append(args, arg...)
		case string:
			args = append(args, arg)
		}
	}
	if len(args) != 0 {
		logFormat += " | ARGS: %s"
		logArgs = append(logArgs, strings.Join(args, " "))
	}
	Log.Infof(logFormat, logArgs...)
}
