// Queue handler and related methods for interacting with the audio queue
package core

import (
	"bytes"
	"crypto/md5"
	"encoding/hex"
	"fmt"
	"math/rand"
	"os"
	"os/exec"
	"strings"
	"sync"
	"time"

	"github.com/jonas747/dca"
)

type (
	QueueHandler struct {
		queues map[string]*Queue
	}
	Queue struct {
		Songs   []Song
		History []string
		Current *Song
		Stream  *dca.StreamingSession
		Repeat  int // 0 = disabled, 1 = queue, 2 = song
		Stop    chan bool
		Lock    sync.Mutex
	}
)

// Returns a new handler for holding queue data
func NewQueueHandler() *QueueHandler {
	return &QueueHandler{make(map[string]*Queue)}
}

// Creates an empty queue and assigns it to a guild
func (handler *QueueHandler) newQueue(guildID string) *Queue {
	queue := &Queue{
		Songs:   make([]Song, 0),
		History: make([]string, 0),
		Stop:    make(chan bool),
		Lock:    sync.Mutex{},
	}
	handler.queues[guildID] = queue
	return queue
}

// Gets a guild's queue, or creates one if it doesn't exist.
func (handler *QueueHandler) GetQueue(guildID string) *Queue {
	queue, f := handler.queues[guildID]
	if !f {
		queue = handler.newQueue(guildID)
	}
	return queue
}

// Gets the next song in the queue
func (queue *Queue) Next() Song {
	queue.Lock.Lock()
	defer queue.Lock.Unlock()

	song := queue.Songs[0]
	queue.Songs = queue.Songs[1:]
	queue.Current = &song
	return song
}

// Adds a song to end of queue
func (queue *Queue) Add(song Song) {
	queue.Lock.Lock()
	defer queue.Lock.Unlock()

	queue.Songs = append(queue.Songs, song)
}

// Adds a slice of songs to the end of the queue
func (queue *Queue) AddN(songs []Song) {
	queue.Lock.Lock()
	defer queue.Lock.Unlock()

	queue.Songs = append(queue.Songs, songs...)
}

// Adds a song to the queue at index `i`
func (queue *Queue) Insert(i int, song Song) {
	queue.Lock.Lock()
	defer queue.Lock.Unlock()

	songs := queue.Songs
	if i >= len(songs) {
		queue.Songs = append(queue.Songs, song)
		return
	}
	if i < 0 {
		i = 0
	}
	songs = append(songs[:i+1], songs[i:]...)
	songs[i] = song
	queue.Songs = songs
}

// Adds a slice of songs to the queue at index `i`
func (queue *Queue) InsertN(i int, songs []Song) {
	queue.Lock.Lock()
	defer queue.Lock.Unlock()

	qSongs := queue.Songs
	if i >= len(qSongs) {
		queue.Songs = append(qSongs, songs...)
		return
	}
	if i < 0 {
		i = 0
	}

	songList := append(qSongs[:i], songs...)
	songList = append(songList, qSongs[i:]...)
	queue.Songs = songList
}

// Removes song at index `i`
func (queue *Queue) Remove(i int) {
	queue.Lock.Lock()
	defer queue.Lock.Unlock()

	queue.Songs = append(queue.Songs[:i], queue.Songs[i+1:]...)
}

// Removes songs up to (but excluding) index `i`
func (queue *Queue) RemoveN(i int) {
	queue.Lock.Lock()
	defer queue.Lock.Unlock()

	if i+1 >= len(queue.Songs) {
		queue.Songs = make([]Song, 0)
		return
	}

	queue.Songs = queue.Songs[i:]
}

// Removes song at index `i` and returns it
func (queue *Queue) Pop(i int) Song {
	queue.Lock.Lock()
	defer queue.Lock.Unlock()

	song := queue.Songs[i]
	queue.Songs = append(queue.Songs[:i], queue.Songs[i+1:]...)
	return song
}

// Returns a copy of queue.Songs
func (queue *Queue) List() []Song {
	queue.Lock.Lock()
	defer queue.Lock.Unlock()

	songList := make([]Song, len(queue.Songs))
	copy(songList, queue.Songs)
	return songList
}

// Shuffles the queue songs
func (queue *Queue) Shuffle() {
	queue.Lock.Lock()
	defer queue.Lock.Unlock()
	q := queue.Songs
	r := rand.New(rand.NewSource(time.Now().UnixNano()))
	for i := len(q) - 1; i > 0; i-- {
		j := r.Intn(i + 1)
		q[i], q[j] = q[j], q[i]
	}
}

// Removes all songs from the queue
func (queue *Queue) Clear() {
	queue.Lock.Lock()
	defer queue.Lock.Unlock()

	queue.Songs = make([]Song, 0)
}

// Returns a bool denoting if the queue is empty
func (queue *Queue) Empty() bool {
	queue.Lock.Lock()
	defer queue.Lock.Unlock()

	return len(queue.Songs) == 0
}

// Starts a loop that stops when the queue is empty
func (queue *Queue) Start(ctx *Context) {
	sess := ctx.Sessions.GetSession(ctx.Guild.ID)

	for !queue.Empty() {
		song := queue.Next()
		sess.Current = &song

		songStr := fmt.Sprintf("[%s](%s)\n", song.Title, song.WebPage)
		offset := 10
		if offset > len(queue.History)+1 {
			offset = len(queue.History) + 1
		}

		queue.History = append(queue.History, songStr)[:offset]
		sess.Connection.Play(ctx, sess)

		if queue.Repeat == 1 {
			queue.Add(song)
		} else if queue.Repeat == 2 {
			queue.Insert(0, song)
		}
	}
	select {
	case <-sess.Closing:
	default:
		ctx.Reply(0x3B88C3, "🎵 Queue complete")
	}
}

type Song struct {
	ID        string
	Title     string
	Thumbnail string
	WebPage   string
	Duration  int
	Requester string
	Path      string
}

// Creates a new song object
func NewSong(v *Video, reqester string) *Song {
	title := v.Title
	titleLower := strings.ToLower(title)
	// remove "(official ...)" from the title
	if strings.Contains(titleLower, "official") {
		tI := strings.Index(titleLower, "official")
		title = strings.TrimSpace(title[:tI-1])
	}

	hasher := md5.New()
	hasher.Write([]byte(v.ID))
	token := hex.EncodeToString(hasher.Sum(nil))

	return &Song{
		v.ID,
		title,
		v.Thumbnail,
		v.WebPage,
		int(v.Duration),
		reqester,
		fmt.Sprintf("cache/%s", token),
	}
}

// Searches YouTubeDL with the given query
func (song Song) Download(ctx *Context) error {
	parameters := downloadParams(&song)
	cmd := exec.Command("yt-dlp", parameters...)
	var output bytes.Buffer
	cmd.Stdout = &output
	return cmd.Run()
}

// Returns YouTubeDL params for downloading
func downloadParams(song *Song) []string {
	params := []string{
		"--source-address", "0.0.0.0",
		"--default-search", "auto",
		"--audio-format", "opus",
		"--no-check-certificate",
		"--format", "bestaudio",
		"--restrict-filenames",
		"--output", song.Path,
		"--extract-audio",
		"--ignore-errors",
		"--no-playlist",
		"--no-warnings",
		"--quiet",
		song.WebPage,
	}
	if _, err := os.Stat("static/cookies.txt"); !os.IsNotExist(err) {
		params = append(params, []string{"--cookies", "static/cookies.txt"}...)
	}
	return params
}
