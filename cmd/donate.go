package cmd

import (
	"fmt"
	"main/core"

	"github.com/bwmarrin/discordgo"
)

func DonateCmd(ctx core.Context) {
	kofiURL := "https://ko-fi.com/shifty9"
	paypalURL := "https://www.paypal.me/shifty9"
	desc := "Want to help? Support via [Ko-Fi](%s) or [PayPal](%s)!"

	embed := &discordgo.MessageEmbed{
		Color: 0x4f208c,
		Author: &discordgo.MessageEmbedAuthor{
			Name:    "Pas Donation Information",
			IconURL: ctx.Session.State.User.AvatarURL("1024"),
		},
		Description: fmt.Sprintf(desc, kofiURL, paypalURL),
	}
	ctx.Reply(embed)
}
