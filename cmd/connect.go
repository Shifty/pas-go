package cmd

import (
	"main/core"

	"github.com/bwmarrin/discordgo"
)

func ConnectCmd(ctx core.Context) {
	userVC, err := ctx.GetVoiceChannel(ctx.Author.ID)
	if err != nil {
		ctx.Reply(0xBE1931, "❗ Failed to retrieve voice channel")
		ctx.Errors.ErrChan <- ctx.Errors.Error(&ctx, err)
		return
	}
	if userVC == nil {
		ctx.Reply(0xBE1931, "❗ You are not in a voice channel")
		return
	}
	botVC, err := ctx.GetVoiceChannel(ctx.Session.State.User.ID)
	if err != nil {
		ctx.Reply(0xBE1931, "❗ Failed to retrieve voice channel")
		ctx.Errors.ErrChan <- ctx.Errors.Error(&ctx, err)
		return
	}
	queue := ctx.Queues.GetQueue(ctx.Guild.ID)
	if botVC != nil {
		if botVC.ID != userVC.ID {
			if queue != nil && queue.Stream != nil {
				ctx.Reply(0xBE1931, "❗ I am already playing in another channel")
				return
			}
		} else {
			ctx.Reply(0xBE1931, "❗ We are already in the same channel")
			return
		}
	}
	perms, err := ctx.Session.State.UserChannelPermissions(ctx.Session.State.User.ID, userVC.ID)
	if err != nil {
		ctx.Reply(0xBE1931, "❗ Failed to retrieve permissions")
		ctx.Errors.ErrChan <- ctx.Errors.Error(&ctx, err)
		return
	}
	if perms&int64(discordgo.PermissionVoiceConnect) == 0 {
		ctx.Reply(0xBE1931, "❗ I cannot connect to that channel")
		return
	}
	if perms&int64(discordgo.PermissionVoiceSpeak) == 0 {
		ctx.Reply(0xBE1931, "❗ I cannot speak in that channel")
		return
	}

	sess := ctx.Sessions.GetSession(ctx.Guild.ID)
	if sess != nil {
		if conn := sess.Connection; conn != nil && conn.VoiceConnection != nil {
			sess.Connection.VoiceConnection.Disconnect()
		}
	}

	err = ctx.Sessions.Join(ctx.Session, ctx.Guild.ID, userVC.ID)
	if err != nil {
		ctx.Reply(0xBE1931, "❗ Failed to connect")
		ctx.Errors.ErrChan <- ctx.Errors.Error(&ctx, err)
		return
	}
	ctx.VoiceChannel = userVC
	ctx.Reply(0x77b255, "✅ Connected to "+userVC.Name)
}
