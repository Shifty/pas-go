// Error handler and related methods for interacting with errors and receiving error events
package core

import (
	"fmt"
	"strings"
	"time"

	"github.com/bwmarrin/discordgo"
	"github.com/go-errors/errors"
)

type (
	ErrorHandler struct {
		debug     bool
		channelID string
		ErrChan   chan CommandError
	}
	CommandError struct {
		ctx   *Context
		err   error
		stack string
	}
)

// Returns a new handler with error data
func NewErrorHandler(cfg *Config) *ErrorHandler {
	handler := &ErrorHandler{
		debug:     cfg.Debug,
		channelID: cfg.ErrorChannelID,
		ErrChan:   make(chan CommandError, 10),
	}
	return handler
}

// Returns a new error instance with stack trace
func (handler *ErrorHandler) Error(ctx *Context, err error) CommandError {
	cmderr := CommandError{ctx: ctx, err: err}
	cmderr.stack = errors.Wrap(err, 1).ErrorStack()
	return cmderr
}

// Sends error data in a formatted message to the configured channel
// Handles errors for legacy commands
func (handler *ErrorHandler) reportLegacy(ce CommandError) {
	ctx := ce.ctx
	if handler.debug {
		logFormat := "%s | %s"
		logArgs := []interface{}{
			strings.ToUpper(ctx.Command.Name),
			ctx.Guild.Name,
			ctx.Guild.ID,
			ctx.TextChannel.Name,
			ctx.TextChannel.ID,
			ctx.Author.Username,
			ctx.Author.Discriminator,
			ctx.Author.ID,
			ce.err.Error(),
		}
		Log.Errorf(logFormat, logArgs...)
	} else if handler.channelID != "" {
		content := fmt.Sprintf("Command **%s** produced an error", ce.ctx.Command.Name)
		content += fmt.Sprintf("\n```go\n%s\n```", ce.stack)
		_, err := ctx.Session.ChannelMessageSend(handler.channelID, content)
		if err != nil {
			Log.Error("failed to send error log: ", err)
		}
	}
}

// Sends error data in a formatted message to the configured channel
// Handles errors for slash commands
func (handler *ErrorHandler) reportSlash(ce CommandError) {
	ctx := ce.ctx
	if handler.debug {
		logFormat := "%s | %s"
		logArgs := []interface{}{
			strings.ToUpper(ctx.Data.ApplicationCommandData().Name),
			ctx.Guild.Name,
			ctx.Guild.ID,
			ctx.TextChannel.Name,
			ctx.TextChannel.ID,
			ctx.Author.Username,
			ctx.Author.Discriminator,
			ctx.Author.ID,
			ce.err.Error(),
		}
		Log.Errorf(logFormat, logArgs...)
	} else if handler.channelID != "" {
		content := fmt.Sprintf("Command **%s** produced an error", ce.ctx.Data.ApplicationCommandData().Name)
		content += fmt.Sprintf("\n```go\n%s\n```", ce.stack)
		_, err := ctx.Session.ChannelMessageSend(handler.channelID, content)
		if err != nil {
			Log.Error("failed to send error log: ", err)
		}
	}
}

// A loop for receiving and reporting errors
func cycle(handler *ErrorHandler) {
	for {
		select {
		case c := <-handler.ErrChan:
			if c.ctx.isSlash {
				handler.reportSlash(c)
			} else {
				handler.reportLegacy(c)
			}
		case <-time.After(time.Second):
		}
	}
}

// Initializes the error handler
func (handler *ErrorHandler) Start(session *discordgo.Session) {
	if handler.debug {
		Log.Info("debug mode enabled. errors will be printed to stdout")
	}
	if handler.channelID != "" && !handler.debug {
		_, err := session.Channel(handler.channelID)
		if err != nil {
			Log.Warn("error log channel not found; errors will not be reported!")
		} else {
			Log.Info("error logger initialized")
		}
	}

	go cycle(handler)
}
