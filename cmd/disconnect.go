package cmd

import "main/core"

func DisconnectCmd(ctx core.Context) {
	userVC, err := ctx.GetVoiceChannel(ctx.Author.ID)
	if err != nil {
		ctx.Reply(0xBE1931, "❗ Failed to retrieve voice channel")
		ctx.Errors.ErrChan <- ctx.Errors.Error(&ctx, err)
		return
	}
	if userVC == nil {
		ctx.Reply(0xBE1931, "❗ You are not in a voice channel")
		return
	}
	botVC, err := ctx.GetVoiceChannel(ctx.Session.State.User.ID)
	if err != nil {
		ctx.Reply(0xBE1931, "❗ Failed to retrieve voice channel")
		ctx.Errors.ErrChan <- ctx.Errors.Error(&ctx, err)
		return
	}
	if botVC == nil {
		ctx.Reply(0xBE1931, "❗ I am not in a voice channel")
		return
	}
	if botVC.ID != userVC.ID {
		ctx.Reply(0xBE1931, "❗ We are not in the same channel")
		return
	}

	sess := ctx.Sessions.GetSession(ctx.Guild.ID)
	sess.Closing <- true

	queue := ctx.Queues.GetQueue(ctx.Guild.ID)
	if stream := queue.Stream; stream != nil {
		queue.Stop <- true
	}
	queue.Clear()

	err = ctx.Sessions.Leave(ctx.Session, sess)
	if err != nil {
		ctx.Reply(0xBE1931, "❗ Error while leaving channel")
		ctx.Errors.ErrChan <- ctx.Errors.Error(&ctx, err)
		return
	}
	ctx.Reply(0x77b255, "✅ Disconnected")
}
