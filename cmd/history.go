package cmd

import (
	"fmt"
	"main/core"

	"github.com/bwmarrin/discordgo"
)

// Reverses the order of a slice
func reverse(s []string) []string {
	for i, j := 0, len(s)-1; i < j; i, j = i+1, j-1 {
		s[i], s[j] = s[j], s[i]
	}
	return s
}

func HistoryCmd(ctx core.Context) {
	queue := ctx.Queues.GetQueue(ctx.Guild.ID)
	if queue == nil || len(queue.History) == 0 {
		ctx.Reply(0x3B88C3, "ℹ️ No track history")
		return
	}

	descStr := ""
	history := queue.History
	for i, song := range reverse(history) {
		descStr += fmt.Sprintf("**%d.** %s", i+1, song)
	}
	embed := &discordgo.MessageEmbed{
		Color:       0x5DADEC,
		Title:       "🎶 Last 10 songs played",
		Description: descStr,
	}
	ctx.Reply(embed)
}
