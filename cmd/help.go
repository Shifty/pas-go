package cmd

import (
	"fmt"
	"main/core"
	"strings"

	"github.com/bwmarrin/discordgo"
)

func HelpCmd(ctx core.Context) {
	if query, f := ctx.Options["query"]; f {
		query := strings.ToLower(query.(string))
		cmd, f := ctx.Commands.Get(query)
		if !f {
			ctx.Reply(0x696969, "🔍 Command not found")
			return
		}
		usage := ctx.Static.Cfg.Prefix + cmd.Name
		if cmd.Usage != "" {
			usage += " " + cmd.Usage
		}
		usageFld := &discordgo.MessageEmbedField{
			Name:   "Usage Example",
			Value:  fmt.Sprintf("`%s`", usage),
			Inline: false,
		}
		descFld := &discordgo.MessageEmbedField{
			Name:   "Description",
			Value:  fmt.Sprintf("```\n%s\n```", cmd.Desc),
			Inline: false,
		}
		embed := &discordgo.MessageEmbed{
			Color: 0x4f208c,
			Title: fmt.Sprintf("📄 %s Help", strings.ToUpper(cmd.Name)),
		}
		embed.Fields = append(embed.Fields, usageFld, descFld)
		if len(cmd.Aliases) > 0 {
			aliasFld := &discordgo.MessageEmbedField{
				Name:   "Aliases",
				Value:  fmt.Sprintf("```\n%s\n```", strings.Join(cmd.Aliases, ", ")),
				Inline: false,
			}
			embed.Fields = append(embed.Fields, aliasFld)
		}
		ctx.Reply(embed)
		return

	} else {
		isOwner := ctx.Static.Cfg.IsOwner(ctx.Author.ID)
		cmdList := ctx.Commands.List(isOwner)
		cmdStr := ""
		for _, name := range cmdList {
			cmdStr += fmt.Sprintf("- %s\n", strings.ToUpper(name))
		}
		embed := &discordgo.MessageEmbed{
			Color: 0x4f208c,
			Fields: []*discordgo.MessageEmbedField{{
				Name:  "Command List",
				Value: fmt.Sprintf("```yml\n%s```", cmdStr),
			}},
			Footer: &discordgo.MessageEmbedFooter{
				Text: fmt.Sprintf("Type %shelp [command] for more help", ctx.Static.Cfg.Prefix),
			},
		}
		ctx.Reply(embed)
	}
}
