package cmd

import (
	"fmt"
	"main/core"
	"time"

	"github.com/bwmarrin/discordgo"
)

func NowPlayingCmd(ctx core.Context) {
	queue := ctx.Queues.GetQueue(ctx.Guild.ID)
	if queue == nil || queue.Current == nil {
		ctx.Reply(0x3B88C3, "🎵 Nothing currently playing")
		return
	}

	song := queue.Current
	duration := time.Duration(int(time.Second) * song.Duration)
	embed := &discordgo.MessageEmbed{
		Color: 0x3B88C3,
		Fields: []*discordgo.MessageEmbedField{{
			Name:  "🎵 Now Playing",
			Value: fmt.Sprintf("[%s](%s)", song.Title, song.WebPage),
		}},
		Thumbnail: &discordgo.MessageEmbedThumbnail{
			URL: song.Thumbnail,
		},
		Footer: &discordgo.MessageEmbedFooter{
			Text: fmt.Sprintf("Duration: %s | Requester: %s", duration.String(), song.Requester),
		},
	}
	ctx.Reply(embed)
}
