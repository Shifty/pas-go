package cmd

import (
	"fmt"
	"main/core"
)

func UnQueueCmd(ctx core.Context) {
	userVC, err := ctx.GetVoiceChannel(ctx.Author.ID)
	if err != nil {
		ctx.Reply(0xBE1931, "❗ Failed to retrieve voice channel")
		ctx.Errors.ErrChan <- ctx.Errors.Error(&ctx, err)
		return
	}
	if userVC == nil {
		ctx.Reply(0xBE1931, "❗ You are not in a voice channel")
		return
	}
	botVC, err := ctx.GetVoiceChannel(ctx.Session.State.User.ID)
	if err != nil {
		ctx.Reply(0xBE1931, "❗ Failed to retrieve voice channel")
		ctx.Errors.ErrChan <- ctx.Errors.Error(&ctx, err)
		return
	}
	if botVC == nil {
		ctx.Reply(0xBE1931, "❗ I am not in a voice channel")
		return
	}
	if botVC.ID != userVC.ID {
		ctx.Reply(0xBE1931, "❗ We are not in the same channel")
		return
	}

	var f bool
	var index int
	if index, f = ctx.Options["index"].(int); !f {
		ctx.Reply(0xBE1931, "❗ Missing input")
		return
	}
	if index == 0 {
		ctx.Reply(0xBE1931, "❗ Input must be positive")
		return
	}

	queue := ctx.Queues.GetQueue(ctx.Guild.ID)
	if len(queue.Songs) == 0 {
		ctx.Reply(0xBE1931, "❗ The queue is empty")
		return
	}
	if index > len(queue.Songs) || index < 0 {
		ctx.Reply(0xBE1931, "❗ Target song is out of range")
		return
	}

	song := queue.Pop(index - 1)
	ctx.Reply(0x66CC66, fmt.Sprintf("✅ Removed %s", song.Title))
}
