package cmd

import (
	"fmt"
	"main/core"
)

func ShuffleCmd(ctx core.Context) {
	userVC, err := ctx.GetVoiceChannel(ctx.Author.ID)
	if err != nil {
		ctx.Reply(0xBE1931, "❗ Failed to retrieve voice channel")
		ctx.Errors.ErrChan <- ctx.Errors.Error(&ctx, err)
		return
	}
	if userVC == nil {
		ctx.Reply(0xBE1931, "❗ You are not in a voice channel")
		return
	}
	botVC, err := ctx.GetVoiceChannel(ctx.Session.State.User.ID)
	if err != nil {
		ctx.Reply(0xBE1931, "❗ Failed to retrieve voice channel")
		ctx.Errors.ErrChan <- ctx.Errors.Error(&ctx, err)
		return
	}
	if botVC == nil {
		ctx.Reply(0xBE1931, "❗ I am not in a voice channel")
		return
	}
	if botVC.ID != userVC.ID {
		ctx.Reply(0xBE1931, "❗ We are not in the same channel")
		return
	}
	queue := ctx.Queues.GetQueue(ctx.Guild.ID)
	if queue.Empty() {
		ctx.Reply(0xBE1931, "❗ The queue is empty")
		return
	}

	queue.Shuffle()
	ctx.Reply(0x3B88C3, fmt.Sprintf("🔀 Shuffled %d songs", len(queue.Songs)))
}
