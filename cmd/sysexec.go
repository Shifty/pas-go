package cmd

import (
	"bytes"
	"fmt"
	"main/core"
	"os/exec"
)

func SysExecCmd(ctx core.Context) {
	if !ctx.Static.Cfg.IsOwner(ctx.Author.ID) {
		ctx.TryReact("⛔")
		return
	}
	if len(ctx.Options) == 0 {
		ctx.Reply(0xBE1931, "❗ Missing input")
		return
	}

	args := ctx.Options["arguments"]
	if args == nil {
		args = []string{}
	}
	cmd := exec.Command(ctx.Options["command"].(string), args.([]string)...)
	var out bytes.Buffer
	cmd.Stdout = &out
	cerr := cmd.Run()
	if cerr != nil {
		ctx.TryReact("❗")
		ctx.Reply(fmt.Sprintf("```\n%s\n```", cerr))
		return
	}

	if out.String() != "" && out.String() != "\n" {
		ctx.Reply(fmt.Sprintf("```\n%s\n```", out.String()))
	} else {
		ctx.TryReact("✔")
	}
}
