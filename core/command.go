// Command handler and related methods for loading and interacting with command data
package core

type (
	CommandHandler struct {
		data cmdMap
		cmds cmdMap
		alts map[string]string
	}
	Command struct {
		Function func(Context)
		Name     string   `json:"name"`
		Usage    string   `json:"usage"`
		Desc     string   `json:"description"`
		Aliases  []string `json:"alts"`
		Owner    bool     `json:"owner"`
	}
	cmdMap = map[string]*Command
)

// Return a new handler with command data
func NewCommandHandler() *CommandHandler {
	info := LoadInfo("static/info.json")
	unreg := make(cmdMap, len(info))
	for _, cmd := range info {
		unreg[cmd.Name] = cmd
	}
	return &CommandHandler{data: unreg, cmds: make(cmdMap), alts: make(map[string]string)}
}

// Get a command by name
func (handler CommandHandler) Get(name string) (*Command, bool) {
	cmd, f := handler.cmds[name]
	if !f {
		alt, f := handler.alts[name]
		if !f {
			return nil, f
		}
		cmd, f := handler.cmds[alt]
		return cmd, f
	}
	return cmd, f
}

// Get a list of all commands
func (handler CommandHandler) List(inclOwner bool) (cmdList []string) {
	if inclOwner {
		for name, cmd := range handler.cmds {
			if !cmd.Owner || inclOwner {
				cmdList = append(cmdList, name)
			}
		}
	}
	return
}

// Processes command data into a usable command object and add it to the handler
func (handler CommandHandler) Register(name string, command func(Context)) {
	cmd, f := handler.data[name]
	if !f {
		Log.Fatalf("no handler for legacy command '%s'", name)
	}
	cmd.Function = command
	handler.cmds[name] = cmd
	if len(cmd.Aliases) != 0 {
		for _, alias := range cmd.Aliases {
			handler.alts[alias] = name
		}
	}
	delete(handler.data, name)
}
