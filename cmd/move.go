package cmd

import (
	"fmt"
	"main/core"
)

func MoveCmd(ctx core.Context) {
	userVC, err := ctx.GetVoiceChannel(ctx.Author.ID)
	if err != nil {
		ctx.Reply(0xBE1931, "❗ Failed to retrieve voice channel")
		ctx.Errors.ErrChan <- ctx.Errors.Error(&ctx, err)
		return
	}
	if userVC == nil {
		ctx.Reply(0xBE1931, "❗ You are not in a voice channel")
		return
	}
	botVC, err := ctx.GetVoiceChannel(ctx.Session.State.User.ID)
	if err != nil {
		ctx.Reply(0xBE1931, "❗ Failed to retrieve voice channel")
		ctx.Errors.ErrChan <- ctx.Errors.Error(&ctx, err)
		return
	}
	if botVC == nil {
		ctx.Reply(0xBE1931, "❗ I am not in a voice channel")
		return
	}
	if botVC.ID != userVC.ID {
		ctx.Reply(0xBE1931, "❗ We are not in the same channel")
		return
	}

	var f bool
	var indexA int
	var indexB int
	if indexA, f = ctx.Options["current-position"].(int); !f {
		ctx.Reply(0xBE1931, "❗ Two arguments are required")
		return
	}
	if indexB, f = ctx.Options["target-position"].(int); !f {
		ctx.Reply(0xBE1931, "❗ Two arguments are required")
		return
	}
	if indexA == 0 || indexB == 0 {
		ctx.Reply(0xBE1931, "❗ Inputs must be positive")
		return
	}

	queue := ctx.Queues.GetQueue(ctx.Guild.ID)
	if len(queue.Songs) == 0 {
		ctx.Reply(0xBE1931, "❗ The queue is empty")
		return
	}
	if indexA > len(queue.Songs) || indexA < 0 {
		ctx.Reply(0xBE1931, "❗ Target song is out of range")
		return
	}
	if indexB > len(queue.Songs) || indexA < 0 {
		ctx.Reply(0xBE1931, "❗ Target position is out of range")
		return
	}
	if indexA == indexB {
		ctx.Reply(0xBE1931, "❗ Target song is already in that position")
		return
	}

	song := queue.Pop(indexA - 1)
	queue.Insert(indexB-1, song)

	ctx.Reply(0x66CC66, fmt.Sprintf("✅ Moved song %d to position %d", indexA, indexB))
}
