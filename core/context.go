// Context handler and related methods for interacting with any data necessary for commands/events
package core

import (
	"github.com/bwmarrin/discordgo"
	"go.uber.org/zap"
)

// This is a "universal" struct that accomodates fields for both legacy and slash commands.
// This was done to maintain compatibility between this struct and the methods that receive it.

type Context struct {
	Session      *discordgo.Session
	Data         *discordgo.InteractionCreate
	Guild        *discordgo.Guild
	VoiceChannel *discordgo.Channel
	TextChannel  *discordgo.Channel
	Message      *discordgo.MessageCreate
	Author       *discordgo.User
	Options      map[string]interface{}
	Log          *zap.SugaredLogger
	Static       *Static
	Command      *Command
	Commands     *CommandHandler
	Errors       *ErrorHandler
	Sessions     *SessionHandler
	Queues       *QueueHandler
	YouTube      *YouTube
	Database     *Database
	isSlash      bool
}

// Return a new handler with the provided data.
func NewContext(
	session *discordgo.Session,
	data *discordgo.InteractionCreate,
	guild *discordgo.Guild,
	channel *discordgo.Channel,
	message *discordgo.MessageCreate,
	author *discordgo.User,
	static *Static,
	command *Command,
	commands *CommandHandler,
	errors *ErrorHandler,
	sessions *SessionHandler,
	queues *QueueHandler,
	youtube *YouTube,
	database *Database,
	isSlash bool) *Context {
	ctx := new(Context)
	ctx.Session = session
	ctx.Data = data
	ctx.Guild = guild
	ctx.TextChannel = channel
	ctx.Message = message
	ctx.Author = author
	ctx.Log = Log
	ctx.Static = static
	ctx.Command = command
	ctx.Commands = commands
	ctx.Errors = errors
	ctx.Sessions = sessions
	ctx.Queues = queues
	ctx.YouTube = youtube
	ctx.Database = database
	ctx.isSlash = isSlash
	return ctx
}

// Tries to react to a message with an emote. Otherwise, sends it as a message.
// Delegates to the appropriate method.
func (ctx *Context) TryReact(emoji string) {
	if !ctx.isSlash {
		ctx.Session.MessageReactionAdd(ctx.Message.ChannelID, ctx.Message.ID, emoji)
	} else {
		ctx.replyL(emoji)
	}
}

// Sends a new message in the context-stored text channel.
// Delegates to the appropriate method.
func (ctx *Context) Reply(other ...interface{}) *discordgo.Message {
	if !ctx.isSlash {
		return ctx.replyL(other...)
	} else {
		ctx.replyS(other...)
		return &discordgo.Message{}
	}
}

// Edits an existing message with the provided message.
// Delegates to the appropriate method.
func (ctx *Context) Edit(message *discordgo.Message, other ...interface{}) *discordgo.Message {
	if !ctx.isSlash {
		return ctx.editL(message, other...)
	} else {
		return ctx.editS(other...)
	}
}

// Checks if the context-stored user has the specified permission.
// Delegates to the appropriate method.
func (ctx *Context) HasPerm(channelID string, userID string, perm int) (bool, error) {
	if !ctx.isSlash {
		return ctx.hasPermL(channelID, userID, perm)
	} else {
		return ctx.hasPermS(channelID, userID, perm)
	}
}

// Retreives the context-stored voice client's channel.
// Delegates to the appropriate method
func (ctx *Context) GetVoiceChannel(userID string) (*discordgo.Channel, error) {
	if !ctx.isSlash {
		return ctx.getVoiceChannelL(userID)
	} else {
		return ctx.getVoiceChannelS(userID)
	}
}

// The edit method for legacy commands.
func (ctx *Context) editL(message *discordgo.Message, other ...interface{}) *discordgo.Message {
	var color int
	var content string
	var embed *discordgo.MessageEmbed

	for _, inter := range other {
		switch inter := inter.(type) {
		case int:
			color = inter
		case string:
			content = inter
		case *discordgo.MessageEmbed:
			embed = inter
		}
	}

	if (content != "" && color != 0) || embed != nil {
		if embed == nil {
			embed = &discordgo.MessageEmbed{
				Color: color,
				Title: content,
			}
		}
		msg, err := ctx.Session.ChannelMessageEditEmbed(message.ChannelID, message.ID, embed)
		if err != nil {
			ctx.Log.Error("failed to edit message: ", err)
			ctx.Errors.ErrChan <- ctx.Errors.Error(ctx, err)
			return nil
		}
		return msg

	} else if content != "" {
		msg, err := ctx.Session.ChannelMessageEdit(message.ChannelID, message.ID, content)
		if err != nil {
			ctx.Log.Error("failed to edit message: ", err)
			ctx.Errors.ErrChan <- ctx.Errors.Error(ctx, err)
			return nil
		}
		return msg
	}

	return nil
}

// The reply method for legacy commands.
func (ctx *Context) replyL(other ...interface{}) *discordgo.Message {
	var color int
	var content string
	var embed *discordgo.MessageEmbed

	for _, inter := range other {
		switch inter := inter.(type) {
		case int:
			color = inter
		case string:
			content = inter
		case *discordgo.MessageEmbed:
			embed = inter
		}
	}

	if (content != "" && color != 0) || embed != nil {
		if embed == nil {
			embed = &discordgo.MessageEmbed{
				Color: color,
				Title: content,
			}
		}

		msg, err := ctx.Session.ChannelMessageSendEmbed(ctx.TextChannel.ID, embed)
		if err != nil {
			ctx.Log.Error("failed to send message: ", err)
			ctx.Errors.ErrChan <- ctx.Errors.Error(ctx, err)
			return nil
		}
		return msg

	} else if content != "" {
		msg, err := ctx.Session.ChannelMessageSend(ctx.TextChannel.ID, content)
		if err != nil {
			ctx.Log.Error("failed to send message: ", err)
			ctx.Errors.ErrChan <- ctx.Errors.Error(ctx, err)
			return nil
		}
		return msg
	}

	return nil
}

// The HasPerm method for legacy commands.
func (ctx *Context) hasPermL(channelID string, userID string, perm int) (bool, error) {
	chnPerm, err := ctx.Session.State.UserChannelPermissions(userID, channelID)
	if err != nil {
		ctx.Log.Error("failed to get permissions: ", err)
		return false, err
	}
	hasPerm := chnPerm&int64(perm) == int64(perm)
	return hasPerm, err
}

// The GetVoiceChannel method for legacy commands.
func (ctx *Context) getVoiceChannelL(userID string) (*discordgo.Channel, error) {
	if state, err := ctx.Session.State.VoiceState(ctx.Guild.ID, userID); state != nil {
		if err != nil {
			ctx.Log.Error("failed to get voice state: ", err)
			return nil, err
		}
		if channel, err := ctx.Session.State.Channel(state.ChannelID); channel != nil {
			if err != nil {
				ctx.Log.Error("failed to get channel: ", err)
			}
			return channel, err
		}
	}
	return nil, nil
}

// The edit method for slash commands.
func (ctx *Context) editS(other ...interface{}) *discordgo.Message {
	var color int
	var content string
	var embed *discordgo.MessageEmbed

	for _, inter := range other {
		switch inter := inter.(type) {
		case int:
			color = inter
		case string:
			content = inter
		case *discordgo.MessageEmbed:
			embed = inter
		}
	}

	if (content != "" && color != 0) || embed != nil {
		if embed == nil {
			embed = &discordgo.MessageEmbed{
				Color: color,
				Title: content,
			}
		}

		msg, err := ctx.Session.InteractionResponseEdit(ctx.Data.Interaction, &discordgo.WebhookEdit{
			Embeds: &[]*discordgo.MessageEmbed{
				embed,
			},
		})
		if err != nil {
			ctx.Log.Error("failed to edit message: ", err)
			ctx.Errors.ErrChan <- ctx.Errors.Error(ctx, err)
			return nil
		}
		return msg

	} else if content != "" {
		msg, err := ctx.Session.InteractionResponseEdit(ctx.Data.Interaction, &discordgo.WebhookEdit{
			Content: &content,
		})
		if err != nil {
			ctx.Log.Error("failed to edit message: ", err)
			ctx.Errors.ErrChan <- ctx.Errors.Error(ctx, err)
			return nil
		}
		return msg
	}

	return nil
}

// The reply method for slash commands.
func (ctx *Context) replyS(other ...interface{}) {
	var color int
	var content string
	var embed *discordgo.MessageEmbed
	var data *discordgo.InteractionResponseData

	for _, inter := range other {
		switch inter := inter.(type) {
		case int:
			color = inter
		case string:
			content = inter
		case *discordgo.MessageEmbed:
			embed = inter
		case *discordgo.InteractionResponseData:
			data = inter
		}
	}

	if data != nil {
		ctx.Session.InteractionRespond(ctx.Data.Interaction, &discordgo.InteractionResponse{
			Type: discordgo.InteractionResponseChannelMessageWithSource,
			Data: data,
		})

	} else if (content != "" && color != 0) || embed != nil {
		if embed == nil {
			embed = &discordgo.MessageEmbed{
				Color: color,
				Title: content,
			}
		}

		ctx.Session.InteractionRespond(ctx.Data.Interaction, &discordgo.InteractionResponse{
			Type: discordgo.InteractionResponseChannelMessageWithSource,
			Data: &discordgo.InteractionResponseData{
				Embeds: []*discordgo.MessageEmbed{
					embed,
				},
			},
		})

	} else if content != "" {
		ctx.Session.InteractionRespond(ctx.Data.Interaction, &discordgo.InteractionResponse{
			Type: discordgo.InteractionResponseChannelMessageWithSource,
			Data: &discordgo.InteractionResponseData{
				Content: content,
			},
		})
	}
}

// Sends a followup message. Exclusive to slash commands.
func (ctx *Context) FollowupS(other ...interface{}) *discordgo.Message {
	var color int
	var content string
	var embed *discordgo.MessageEmbed

	for _, inter := range other {
		switch inter := inter.(type) {
		case int:
			color = inter
		case string:
			content = inter
		case *discordgo.MessageEmbed:
			embed = inter
		}
	}

	if (content != "" && color != 0) || embed != nil {
		if embed == nil {
			embed = &discordgo.MessageEmbed{
				Color: color,
				Title: content,
			}
		}

		msg, err := ctx.Session.FollowupMessageCreate(ctx.Data.Interaction, true, &discordgo.WebhookParams{
			Embeds: []*discordgo.MessageEmbed{
				embed,
			},
		})
		if err != nil {
			ctx.Log.Error("failed to edit message: ", err)
			ctx.Errors.ErrChan <- ctx.Errors.Error(ctx, err)
			return nil
		}
		return msg

	} else if content != "" {
		msg, err := ctx.Session.FollowupMessageCreate(ctx.Data.Interaction, true, &discordgo.WebhookParams{
			Content: content,
		})
		if err != nil {
			ctx.Log.Error("failed to edit message: ", err)
			ctx.Errors.ErrChan <- ctx.Errors.Error(ctx, err)
			return nil
		}
		return msg
	}

	return nil
}

// The HasPerm method for slash commands.
func (ctx *Context) hasPermS(channelID string, userID string, perm int) (bool, error) {
	chnPerm, err := ctx.Session.State.UserChannelPermissions(userID, channelID)
	if err != nil {
		ctx.Log.Error("failed to get permissions: ", err)
		return false, err
	}
	hasPerm := chnPerm&int64(perm) == int64(perm)
	return hasPerm, err
}

// The GetVoiceChannel method for slash commands.
func (ctx *Context) getVoiceChannelS(userID string) (*discordgo.Channel, error) {
	if state, err := ctx.Session.State.VoiceState(ctx.Guild.ID, userID); state != nil {
		if err != nil {
			ctx.Log.Error("failed to get voice state: ", err)
			return nil, err
		}
		if channel, err := ctx.Session.State.Channel(state.ChannelID); channel != nil {
			if err != nil {
				ctx.Log.Error("failed to get channel: ", err)
			}
			return channel, err
		}
	}
	return nil, nil
}
