package cmd

import "main/core"

func PauseCmd(ctx core.Context) {
	userVC, err := ctx.GetVoiceChannel(ctx.Author.ID)
	if err != nil {
		ctx.Reply(0xBE1931, "❗ Failed to get voice channel")
		ctx.Errors.ErrChan <- ctx.Errors.Error(&ctx, err)
		return
	}
	if userVC == nil {
		ctx.Reply(0xBE1931, "❗ You are not in a voice channel")
		return
	}
	botVC, err := ctx.GetVoiceChannel(ctx.Session.State.User.ID)
	if err != nil {
		ctx.Reply(0xBE1931, "❗ Failed to get voice channel")
		ctx.Errors.ErrChan <- ctx.Errors.Error(&ctx, err)
		return
	}
	if botVC == nil {
		ctx.Reply(0xBE1931, "❗ I am not in a voice channel")
		return
	}
	if botVC.ID != userVC.ID {
		ctx.Reply(0xBE1931, "❗ We are not in the same channel")
		return
	}

	queue := ctx.Queues.GetQueue(ctx.Guild.ID)
	queue.Lock.Lock()
	defer queue.Lock.Unlock()

	if stream := queue.Stream; stream != nil {
		if !stream.Paused() {
			queue.Stream.SetPaused(true)
			ctx.Reply(0x3B88C3, "⏸ Music paused")
		} else {
			ctx.Reply(0xBE1931, "❗ The player is not active")
		}
	} else {
		ctx.Reply(0xBE1931, "❗ There is no player")
	}
}
