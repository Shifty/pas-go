package cmd

import (
	"fmt"
	"main/core"

	"github.com/bwmarrin/discordgo"
	"github.com/robertkrimen/otto"
)

const exeIcon = "https://i.imgur.com/Lw7mmnX.png"
const errIcon = "https://i.imgur.com/S7aUuLU.png"

var vm = otto.New()

func EvalCmd(ctx core.Context) {
	if !ctx.Static.Cfg.IsOwner(ctx.Author.ID) {
		ctx.TryReact("⛔")
		return
	}
	if len(ctx.Options) == 0 {
		ctx.Reply(0xBE1931, "❗ Missing input")
		return
	}

	// nesting these so `ctx` is in scope
	guild := func(gID string) *discordgo.Guild {
		guild, err := ctx.Session.State.Guild(gID)
		if err != nil {
			ctx.Log.Error("failed to get guild: ", err)
			return nil
		}
		return guild
	}
	member := func(gID, uID string) *discordgo.Member {
		member, err := ctx.Session.GuildMember(gID, uID)
		if err != nil {
			ctx.Log.Error("failed to get member: ", err)
			return nil
		}
		return member
	}
	emojis := func(guildID string) []string {
		guild := guild(guildID)
		arr := make([]string, 0)
		if guild != nil {
			for _, emoj := range guild.Emojis {
				arr = append(arr, "<:"+emoj.Name+":"+emoj.ID+">")
			}
		}
		return arr
	}

	vm.Set("ctx", ctx)
	vm.Set("getGuild", guild)
	vm.Set("getMember", member)
	vm.Set("getEmojis", emojis)

	js := ctx.Options["code"]
	val, err := vm.Run(js)
	if err != nil {
		embed := &discordgo.MessageEmbed{
			Color: 0xBE1931,
			Author: &discordgo.MessageEmbedAuthor{
				Name:    "Error",
				IconURL: errIcon,
			},
			Description: err.Error(),
		}
		ctx.Reply(embed)
		return
	}

	if val.IsNull() {
		ctx.TryReact("✔")
		return
	}

	embed := &discordgo.MessageEmbed{
		Color: 0x38BE6E,
		Author: &discordgo.MessageEmbedAuthor{
			Name:    "Executed",
			IconURL: exeIcon,
		},
		Description: fmt.Sprintf("```go\n%s\n```", val.String()),
	}
	ctx.Reply(embed)
}
