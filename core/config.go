// Configuration handler for loading and interacting with config data
package core

import (
	"encoding/json"
	"os"

	"go.uber.org/zap"
)

type (
	Static struct {
		Cfg *Config
		Ver *Version
	}
	Config struct {
		Debug          bool     `json:"debug"`         // TODO add debug logs
		Mode           int      `json:"mode"`          // 0 legacy, 1 slash, 2 both (default 0)
		Prefix         string   `json:"prefix"`        // Command prefix to respond to
		Token          string   `json:"token"`         // Application token
		Owners         []string `json:"owners"`        // IDs of all bot owners
		Status         string   `json:"status"`        // Activity status to display
		StatusType     int      `json:"status_type"`   // 0 playing, 1 listening (default 0)
		ErrorChannelID string   `json:"error_channel"` // ID of the error log channel
		DB             struct {
			Auth bool   `json:"auth"`
			Name string `json:"name"`
			User string `json:"user"`
			Pass string `json:"pass"`
			Host string `json:"host"`
			Port string `json:"port"`
		} `json:"database"`
	}
	Version struct {
		BuildDate int64  `json:"build_date"`
		Version   string `json:"version"`
	}
)

// Processes config data into a usable object
func LoadStatic() *Static {
	cfg := loadConfig("static/config.json")
	if cfg.Debug {
		Atom.SetLevel(zap.DebugLevel)
	}
	ver := loadVersion("static/version.json")
	return &Static{cfg, ver}
}

// Loads the config file
func loadConfig(filepath string) *Config {
	body, err := os.ReadFile(filepath)
	if err != nil {
		Log.Fatal(err)
	}
	var data Config
	json.Unmarshal(body, &data)
	return &data
}

// Loads the version file
func loadVersion(filepath string) *Version {
	body, err := os.ReadFile(filepath)
	if err != nil {
		Log.Fatal(err)
	}
	var data Version
	json.Unmarshal(body, &data)
	return &data
}

// Loads the info (command data) file
func LoadInfo(filepath string) []*Command {
	body, err := os.ReadFile(filepath)
	if err != nil {
		Log.Fatal(err)
	}
	var data []*Command
	json.Unmarshal(body, &data)
	return data
}

// Returns if the user is an owner of the bot
func (config *Config) IsOwner(userID string) bool {
	for _, oID := range config.Owners {
		if userID == oID {
			return true
		}
	}
	return false

}
