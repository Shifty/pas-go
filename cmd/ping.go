package cmd

import (
	"fmt"
	"main/core"
	"strconv"

	"github.com/bwmarrin/discordgo"
)

func PingCmd(ctx core.Context) {
	latInt := ctx.Session.HeartbeatLatency().Milliseconds()
	latStr := strconv.Itoa(int(latInt))
	embed := &discordgo.MessageEmbed{
		Color: 0x3B88C3,
		Title: fmt.Sprintf("📶 **Ping %sms**", latStr),
	}
	ctx.Reply(embed)
}
