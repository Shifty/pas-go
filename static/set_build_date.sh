#!/usr/bin/env bash

# This is just an easier way for me to bump the build date.

cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null
! [[ -f "version.json" ]] && echo "version file not found" && exit 1

builddate=$(grep -oP "\d{9,}" version.json)
timestamp=$(date +%s)

sed -i "s/$builddate/$timestamp/g" version.json
[[ $? -eq 0 ]] && echo "build date set to ${builddate}" || "error setting build date"
