package cmd

import (
	"fmt"
	"main/core"
	"strings"
	"time"
	"unicode/utf8"

	"github.com/bwmarrin/discordgo"
)

// Truncates a string to the specified length
func truncate(str string, length int) string {
	if length <= 0 {
		return "undefined"
	}
	if utf8.RuneCountInString(str) < length {
		return str
	}
	return string([]rune(str)[:length]) + "..."
}

// Splits a slice into `span` pages and returns the specified page
func paginate(items []core.Song, pageNum int, span int) ([]core.Song, int) {
	if len(items) <= span {
		return items, 1
	}
	pages, length := len(items)/span, len(items)
	var maxPage int
	if pages%span == 0 && length != 0 {
		maxPage = pages
	} else {
		maxPage = pages + 1
	}
	var page int
	if page > maxPage && maxPage != 0 {
		page = maxPage
	} else {
		page = pageNum
	}
	startRange, endRange := (page-1)+span, page+span
	return items[startRange:endRange], page
}

func QueueCmd(ctx core.Context) {
	page := 1
	var pageSet bool
	if pg, f := ctx.Options["page"].(int); f {
		if pg <= 0 {
			pg = 1
		}
		page = pg - 1
		pageSet = true
	}

	if len(ctx.Options) == 0 || pageSet {
		queue := ctx.Queues.GetQueue(ctx.Guild.ID)
		if queue.Empty() {
			embed := &discordgo.MessageEmbed{Color: 0x3B88C3, Title: "🎵 The queue is empty"}
			if curr := queue.Current; curr != nil {
				embed.Description = fmt.Sprintf("Currently playing: [%s](%s)", curr.Title, curr.WebPage)
			}
			ctx.Reply(embed)
			return
		}

		var queueStr string
		if curr := queue.Current; curr != nil {
			queueStr = fmt.Sprintf("Currently playing: [%s](%s)\n\n", curr.Title, curr.WebPage)
		}

		fullSongList := queue.List()
		songList, page := paginate(fullSongList, page, 10)
		startRange := (page - 1) * 10
		totalDur := 0

		for i, song := range songList {
			totalDur += song.Duration
			title := song.Title
			// remove band name from title. makes for a shorter string
			// if strings.Contains(title, "-") {
			// 	tI := strings.Index(title, "-")
			// 	title = title[tI+1:]
			// }
			title = truncate(title, 45)
			songDurStr := time.Duration(int(time.Second) * totalDur).String()
			queueStr += fmt.Sprintf("**%d.** [%s](%s) %s\n", startRange+i+1, title, song.WebPage, songDurStr)
		}

		totalDurStr := time.Duration(int(time.Second) * totalDur).String()
		footerStr := fmt.Sprintf("Songs: %d | Duration: %s | Page: %d", len(fullSongList), totalDurStr, page)

		if queue.Repeat == 1 {
			footerStr += " | Queue repeat on"
		} else if queue.Repeat == 2 {
			footerStr += " | Song repeat on"
		}

		embed := &discordgo.MessageEmbed{
			Color:       0x3B88C3,
			Title:       "🎵 Music Queue",
			Description: queueStr,
			Footer:      &discordgo.MessageEmbedFooter{Text: footerStr},
		}
		ctx.Reply(embed)
		return

	} else {
		userVC, err := ctx.GetVoiceChannel(ctx.Author.ID)
		if err != nil {
			ctx.Reply(0xBE1931, "❗ Failed to retrieve voice channel")
			ctx.Errors.ErrChan <- ctx.Errors.Error(&ctx, err)
			return
		}
		if userVC == nil {
			ctx.Reply(0xBE1931, "❗ You are not in a voice channel")
			return
		}
		botVC, err := ctx.GetVoiceChannel(ctx.Session.State.User.ID)
		if err != nil {
			ctx.Reply(0xBE1931, "❗ Failed to retrieve voice channel")
			ctx.Errors.ErrChan <- ctx.Errors.Error(&ctx, err)
			return
		}
		if botVC == nil {
			ctx.Reply(0xBE1931, "❗ I am not in a voice channel")
			return
		}
		if botVC.ID != userVC.ID {
			ctx.Reply(0xBE1931, "❗ We are not in the same channel")
			return
		}

		query := ctx.Options["query"].(string)
		var msg *discordgo.Message
		queue := ctx.Queues.GetQueue(ctx.Guild.ID)

		if strings.Contains(query, "/watch?") || !strings.HasPrefix(query, "http") {
			msg := ctx.Reply(0xFFCC66, "💽 Queueing song...")

			video, err := ctx.YouTube.GetVideo(query)
			if err != nil {
				ctx.Reply(0xBE1931, "❗ Failed to retrieve video")
				ctx.Errors.ErrChan <- ctx.Errors.Error(&ctx, err)
				return
			}
			if video == nil {
				ctx.Edit(msg, 0x696969, "🔍 No results")
				return
			}

			song := core.NewSong(video, ctx.Author.Username)
			queue.Add(*song)

			duration := time.Duration(int(time.Second) * song.Duration)
			embed := &discordgo.MessageEmbed{
				Color: 0x66CC66,
				Fields: []*discordgo.MessageEmbedField{{
					Name:  "✅ Queued song",
					Value: fmt.Sprintf("[%s](%s)", song.Title, song.WebPage),
				}},
				Thumbnail: &discordgo.MessageEmbedThumbnail{URL: song.Thumbnail},
				Footer:    &discordgo.MessageEmbedFooter{Text: "Duration: " + duration.String()},
			}
			ctx.Edit(msg, embed)

		} else if strings.Contains(query, "/playlist?") {
			msg = ctx.Reply(0xFFCC66, "💽 Queueing playlist...")
			playlist, err := ctx.YouTube.GetPlaylist(query)
			if err != nil {
				ctx.Edit(msg, 0xBE1931, "❗ Failed to retrieve playlist")
				ctx.Errors.ErrChan <- ctx.Errors.Error(&ctx, err)
				return
			}
			if playlist == nil {
				ctx.Edit(msg, 0x696969, "🔍 No results")
				return
			}

			pSongs := make([]core.Song, len(playlist.Entries))
			for i, v := range playlist.Entries {
				pSongs[i] = *core.NewSong(v, ctx.Author.Username)
			}
			queue.AddN(pSongs)
			ctx.Edit(msg, 0x66CC66, fmt.Sprintf("✅ Queued %d songs", len(playlist.Entries)))

		} else {
			ctx.Reply(0xBE1931, "❗ Could not parse input")
		}
	}
}
