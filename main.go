// Pas-Go project main file
package main

import (
	"os"
	"os/signal"
	"syscall"

	"main/core"

	"github.com/bwmarrin/discordgo"
)

var (
	booted   bool
	static   *core.Static
	cfg      *core.Config
	commands *core.CommandHandler
	errors   *core.ErrorHandler
	sessions *core.SessionHandler
	queues   *core.QueueHandler
	youtube  *core.YouTube
	database *core.Database

	readyChan chan bool
)

// Project init function; executed before main()
func init() {
	booted = false
	static = core.LoadStatic()
	cfg = static.Cfg
	commands = core.NewCommandHandler()
	database = core.NewDatabaseHandler(cfg)
	errors = core.NewErrorHandler(cfg)
	queues = core.NewQueueHandler()
	sessions = core.NewSessionHandler()
	youtube = &core.YouTube{}

	readyChan = make(chan bool)
}

// Sets the booting status
func initStatus(session *discordgo.Session) {
	status := discordgo.UpdateStatusData{
		Status: "dnd",
		Activities: []*discordgo.Activity{{
			Name: "booting...",
			Type: 0,
		}},
	}
	session.UpdateStatusComplex(status)
}

// Sets the configured status
func setStatus(session *discordgo.Session) {
	if cfg.Status != "" {
		switch cfg.StatusType {
		case 0:
			session.UpdateGameStatus(0, cfg.Status)
		case 1:
			session.UpdateListeningStatus(cfg.Status)
		default:
			session.UpdateGameStatus(0, "")
		}
	}
}

// Project main function
func main() {
	session, err := discordgo.New("Bot " + cfg.Token)
	if err != nil {
		core.Log.Fatal("error creating session: ", err)
	}

	switch cfg.Mode {
	case 0:
		session.AddHandler(messageCreate)
	case 1:
		session.AddHandler(slashHandler)
	case 2:
		session.AddHandler(slashHandler)
		session.AddHandler(messageCreate)
	default:
		core.Log.Fatal("invalid mode set in config. must be one of: 0, 1, 2")
	}
	session.AddHandler(voiceStateUpdate)
	session.AddHandler(ready)
	session.Identify.Intents = discordgo.IntentsAll
	err = session.Open()
	defer session.Close()
	// defer cleanUp(session)

	if err != nil {
		core.Log.Fatal("error opening websocket: ", err)
	}

	<-readyChan
	initStatus(session)
	errors.Start(session)
	initCommands(session)
	setStatus(session)

	booted = true
	sc := make(chan os.Signal, 1)
	signal.Notify(sc, syscall.SIGINT, syscall.SIGTERM, os.Interrupt)
	<-sc
}

// Registers legacy and slash commands according to config
func initCommands(session *discordgo.Session) {
	switch cfg.Mode {
	case 0:
		initLegacyCommands(session)
	case 1:
		initSlashCommands(session)
	case 2:
		initSlashCommands(session)
		initLegacyCommands(session)
	default:
		core.Log.Fatal("invalid mode set in config. must be one of: 0, 1, 2")
	}
}

// Registers all slash commands
func initSlashCommands(session *discordgo.Session) {
	core.Log.Info("registering slash commands...")
	registered := make(map[string]struct{})
	rc, _ := session.ApplicationCommands(session.State.User.ID, "")
	for _, cmd := range rc {
		registered[cmd.Name] = struct{}{}
	}
	for _, model := range SlashModels {
		if _, ok := registered[model.Name]; !ok {
			_, err := session.ApplicationCommandCreate(session.State.User.ID, "", model)
			if err != nil {
				core.Log.Fatalf("error registering slash command '%s': %v", model.Name, err)
			}
		}
	}
	core.Log.Infof("registered %d slash commands", len(SlashModels))
}

// Registers all legacy commands
func initLegacyCommands(session *discordgo.Session) {
	core.Log.Info("registering legacy commands...")
	for name, command := range Commands {
		commands.Register(name, command)
	}
	core.Log.Infof("registered %d legacy commands", len(Commands))
}

// Apparently there's a rate limit on creating these (5/20s)
// as well as a "daily limit" of which I could not find details on

// Run any necessary cleanup before stopping
// func cleanUp(session *discordgo.Session) {
// 	if cfg.Slash {
// 		core.Log.Info("removing slash commands...")
// 		registeredCommands, err := session.ApplicationCommands(session.State.User.ID, "")
// 		if err != nil {
// 			core.Log.Error("error fetching slash commands: ", err)
// 		}
// 		for _, v := range registeredCommands {
// 			err := session.ApplicationCommandDelete(session.State.User.ID, "", v.ID)
// 			if err != nil {
// 				core.Log.Errorf("error deleting slash command `%s`: ", v.Name, err)
// 			}
// 		}
// 	}
// }
