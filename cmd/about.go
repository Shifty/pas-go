package cmd

import (
	"fmt"
	"main/core"
	"runtime"
	"time"

	"github.com/bwmarrin/discordgo"
)

func AboutCmd(ctx core.Context) {
	repoURL := "https://gitlab.com/Shifty/pas-go"
	kofiURL := "https://ko-fi.com/shifty9"
	paypalURL := "https://www.paypal.me/shifty9"

	goVer := runtime.Version()
	buildDate := time.Unix(ctx.Static.Ver.BuildDate, 10).Format("2006-01-02")
	abtStr := fmt.Sprintf("Version: **%s**\n", ctx.Static.Ver.Version)
	abtStr += fmt.Sprintf("Latest Build: **%s**\n", buildDate)
	abtStr += fmt.Sprintf("Language **%s**\n", goVer[:2]+" "+goVer[2:]) // gox.x.x -> go x.x.x
	abtStr += fmt.Sprintf("Library **discordgo %s**\n", discordgo.VERSION)
	abtStr += fmt.Sprintf("Links: [GitLab](%s) | [Ko-Fi](%s) | [PayPal](%s)", repoURL, kofiURL, paypalURL)

	embed := &discordgo.MessageEmbed{
		Color: 0x4f208c,
		Fields: []*discordgo.MessageEmbedField{{
			Name:  "Personal Audio Streamer",
			Value: abtStr,
		}},
		Thumbnail: &discordgo.MessageEmbedThumbnail{
			URL: ctx.Session.State.User.AvatarURL("1024"),
		},
	}
	ctx.Reply(embed)
}
